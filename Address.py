# -*- coding: utf-8 -*-
import sys
from pathlib import Path
import re

file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

from py_ru_msk_addresses.Street import Street
from py_ru_msk_addresses.Lane import Lane
from py_ru_msk_addresses.Driveway import Driveway
from py_ru_msk_addresses.Boulevard import Boulevard
from py_ru_msk_addresses.Square import Square
from py_ru_msk_addresses.Highway import Highway
from py_ru_msk_addresses.Avenue import Avenue
from py_ru_msk_addresses.Station import Station
from py_ru_msk_addresses.Promenade import Promenade
from py_ru_msk_addresses.Metro import MetroStation, metro_skip_list

'''
Класс предназначен для распознавания структуры адреса по строке, его определяющей.
Класс реализует шаблон проектирования "делегирование", делегируя распознавание связанным классам.
'''

'''Добавить
n_ = ['Домодедово аэропорт', 'Капотня 2-й квартал', 'Капотня 3-й квартал',
      'Новоясеневский тупик', 'Лучевой 1-й просека', 'Капотня 5-й квартал', 'Капотня 4-й квартал',
      'Капотня 1-й квартал', 'Казанский 1-й просека', 'Зеленоград', 'Внуково Поселок',
      'Чоботовская 9-я аллея', 'Земляной Вал', 'Лучевой 2-й просека', 'Березовая Аллея',
      'Проектируемый проезд N 4386', 'Ильинский сквер', 'Маросейка',
      'Ольховский тупик', 'Лучевой 5-й просека', 'Ордынский тупик', 'Мосренген поселок',
      'поселок Филлимонки', 'Проектируемый проезд № 5402', 'Северное Чертаново микрорайон', 'Путейский тупик']
'Бутовская улица' # Нет в OSM
'''




def get_name_in_list(street, streets_list, exceptions = None):
    '''
    Ищет написание улицы в списке

    :param street: Строка названия улицы
    :param streets_list: Список названий улиц
    :return: Название улицы из списка названий
    '''
    for street_from_list in streets_list:
        if is_streets_equals(street, street_from_list, exceptions):
            sn = Address(street_from_list)
            sn.parse()
            return sn.normalize()
    return None

def is_streets_equals(street1, street2, exceptions = None):
    '''
    Сравнивает написания названий улиц
    :param street1: Строка1 названия улицы
    :param street2: Строка2 названия улицы
    :param exceptions: Словарь исключений написаний строки1. Написания строки1, найденные в словаре заменяются соответствующими значениями из словаря.
    :return: (True|False)
    '''

    def is_streets_equals(street1, street2, exceptions=None):
        '''
        Сравнивает написания названий улиц
        :param street1: Строка1 названия улицы
        :param street2: Строка2 названия улицы
        :param exceptions: Словарь исключений написаний строки1. Написания строки1, найденные в словаре заменяются соответствующими значениями из словаря.
        :return: (True|False)
        '''
        if exceptions != None:
            if street1 in exceptions:
                street1 = exceptions[street1]
        sn1 = Address(street1)
        sn1.parse()
        sn2 = Address(street2)
        sn2.parse()
        if sn1.type == sn2.type:
            if hasattr(sn1, 'is_doubler') and hasattr(sn2, 'is_doubler'):
                if sn1.is_doubler != sn2.is_doubler:
                    return False
            if hasattr(sn1, 'is_numeral') and hasattr(sn2, 'is_numeral'):
                if sn1.is_numeral != sn2.is_numeral:
                    return False
                else:
                    if sn1.numeral != sn2.numeral:
                        return False
            if sn1.is_recognized and sn2.is_recognized:
                if sn1.name.lower() != sn2.name.lower():
                    return False
            else:
                return False
        else:
            return False

        return True


class Address(object):
    obj = None
    type = 'undefined'

    is_recognized = False

    classes = {
        "street": "Street",
        "metro-station": "MetroStation",
        "lane": "Lane",
        "driveway": "Driveway",
        "alley": "Alley",
        "square": "Square",
        "highway": "Highway",
        "avenue": "Avenue",
        "blind_alley": "BlindAlley",
        "line": "Line",
        "boulevard": "Boulevard",
        "station": "Station",
        "promenade": "Promenade",
        "glade": "Glade",
        "airport": "Airport",
        "quarter": "Quarter",
        "village": "Village",
        "public_garden": "PublicGarden",
        "bridge": "Bridge",
        "overpass": "Overpass",
        "tunnel": "Tonnel",
        "flyover": "Flyover",
        "rokada": "Rokada",
        "chord": "Chord",
        "ring": "Ring",
        "trail": "Trail",
        "kilometer": "Kilometer",
        "dam": "Dam"
    }

    # Это временное ограничение по типам
    defined_types = [
        'metro-station',
        'street',
        'lane',
        'driveway',
        'boulevard',
        'square',
        'highway',
        'avenue',
        'station',
        'promenade'
    ]

    types = {
        "street": "улицы",
        "metro-station": "станции метро",
        "lane": "переулки",
        "driveway": "проезды",
        "alley": "аллеи",
        "square": "площади",
        "highway": "шоссе",
        "avenue": "проспекты",
        "blind_alley": "тупики",
        "line": "линии",
        "boulevard": "бульвары",
        "station": "вокзалы",
        "promenade": "набережные",
        "glade": "просеки",
        "airport": "аэропорты",
        "quarter": "кварталы",
        "village": "посёлки",
        "public_garden": "скверы",
        "bridge": "мосты",
        "overpass": "путепроводы",
        "tunnel": "тоннели",
        "flyover": "эсткады",
        "rokada": "рокады",
        "chord": "хорды",
        "ring": "кольца",
        "trail": "тропы",
        "kilometer": "километры",
        "dam": "плотины"
    }

    search_patterns = {
        "metro-station": "(?:^|\s+)(?:метро|ст.м.|ст. м.|ст м|ст м.|ст.м|ст м.|м.|м)(?:$|\s+)",
        "street": "(?:^|\s+)(?:улица|ул.|ул)(?:$|\s+)",
        "lane": "(?:^|\s+)(?:переулок|переул.|переул|пер.|пер)(?:$|\s+)",
        "highway": "(?:^|\s+)(?:шоссе|ш.|ш)(?:$|\s+)",
        "avenue": "(?:^|\s+)(?:проспект|просп.|просп)(?:$|\s+)",
        "blind_alley": "(?:^|\s+)(?:тупик|туп.|туп)(?:$|\s+)",
        "alley": "(?:^|\s+)(?:аллея|алея)(?:$|\s+)",
        "line": "(?:^|\s+)(?:линия|лин.|лин)(?:$|\s+)",
        "square": "(?:^|\s+)(?:площадь|пл.|пл)(?:$|\s+)",
        "driveway": "(?:^|\s+)(?:проезд|пр.|пр)(?:$|\s+)",
        "boulevard": "(?:^|\s+)(?:бульвар|б-р.|б-р|бульв.|бульв|бул.|бул|бл.|бл)(?:$|\s+)",
        "station": "(?:^|\s+)(?:вокзал|вкз.|вкз)(?:$|\s+)",
        "promenade": "(?:^|\s+)(?:набережная|набер.|набер|наб.|наб)(?:$|\s+)",
        "glade": "(?:^|\s+)(?:просек|просека)(?:$|\s+)",
        "airport": "(?:^|\s+)(?:аэропорт)(?:$|\s+)",
        "quarter": "(?:^|\s+)(?:квартал|кварт.|кварт)(?:$|\s+)",
        "village": "(?:^|\s+)(?:посёлок|поселок|пос.|пос|п.|п)(?:$|\s+)",
        "public_garden": "(?:^|\s+)(?:сквер|скв.|скв)(?:$|\s+)",
        "bridge": "(?:^|\s+)(?:мост|мостик|метромост)(?:$|\s+)",
        "overpass": "(?:^|\s+)(?:путепровод)(?:$|\s+)",
        "tunnel": "(?:^|\s+)(?:тоннель)(?:$|\s+)",
        "flyover": "(?:^|\s+)(?:эстакада)(?:$|\s+)",
        "rokada": "(?:^|\s+)(?:рокада)(?:$|\s+)",
        "chord": "(?:^|\s+)(?:хорда)(?:$|\s+)",
        "ring": "(?:^|\s+)(?:кольцо)(?:$|\s+)",
        "trail": "(?:^|\s+)(?:тропа)(?:$|\s+)",
        "kilometer": "(?:^|\s+)(?:километр|км.|км)(?:$|\s+)",
        "dam": "(?:^|\s+)(?:плотина)(?:$|\s+)",
    }

    def __init__(self, string, metro_stations=None):
        self.string = string
        flags = re.IGNORECASE + re.UNICODE

        is_in_stations_list = False
        tmp = re.sub(r'\s+', ' ', string.strip())
        if not tmp in metro_skip_list:
            # Станции метро по списку написаний
            if metro_stations is not None:
                self.metro_stations = metro_stations
                #tmp = re.sub(r'\s+', ' ', string.strip())
                #print(tmp)
                for item in metro_stations:
                    #print(tmp.lower(), item.lower())
                    if tmp.lower() == item.lower():
                        is_in_stations_list = True
                        self.type = 'metro-station'
                        break
        #print(string, is_in_stations_list)

        if not is_in_stations_list:
            # Определение типа строки
            for key, search_pattern in self.search_patterns.items():
                pattern = re.compile(search_pattern, flags=flags)
                m = pattern.search(string)
                if m is not None:
                    self.type = key
                    break

    def parse(self):
        obj = None
        if self.type != 'undefined':
            if self.type in self.defined_types:
                # Дополнительная проверка типа - не очень хорошо
                if self.type == 'metro-station':
                    obj = globals()[self.classes[self.type]](self.string, self.metro_stations)
                else:
                    obj = globals()[self.classes[self.type]](self.string)
        if obj is not None:
            self.obj = obj
            self.name = obj.name
            self.is_recognized = obj.is_recognized
            if self.type == 'square':
                self.is_near_the_station = obj.is_near_the_station
                if obj.is_near_the_station:
                    self.station = obj.station
            
        return obj

    def normalize(self):
        if self.obj is not None:
            if self.obj.is_recognized:
                return self.obj.normalize()
        else:
            return None




