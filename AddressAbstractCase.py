# -*- coding: utf-8 -*-
import unittest

'''
Абстрактный класс теста
'''

class AbstractAddressTest(unittest.TestCase):

    def substitute_numerals(self, string, numerals):
        tmp = []
        for numeral in numerals:
            str_case = string.format(numeral=numeral)
            tmp.append(str_case)
        return tmp

    def abbreviations(self, string, subst_word, abbrev, with_brackets=False):
        tmp = []
        for abbr in abbrev:
            tmp.append(string.replace(subst_word, abbr))
            tmp.append(string.replace(subst_word, abbr.upper()))
            tmp.append(string.replace(subst_word, abbr.capitalize()))
        if with_brackets:
            for abbr in abbrev:
                tmp.append(string.replace(subst_word, '('+ abbr + ')'))
                tmp.append(string.replace(subst_word, '('+ abbr.upper() + ')'))
                tmp.append(string.replace(subst_word, '('+ abbr.capitalize() + ')'))
        return tmp

    def brackets(self, string, substr_word):
        return string.replace(substr_word, '(' + substr_word + ')')

    def spaces(self, string):
        parts = string.split(' ')
        str_case = "     " + "     ".join(parts) + "     "
        return str_case