# -*- coding: utf-8 -*-
import re


class Avenue(object):

    is_recognized = False
    is_numeral = False
    is_doubler = False

    name = None
    numeral = ''

    '''
    Волгоградский проспект(дублёр)
    проспект Защитников Москвы(боковой проезд)
    проспект Маршала Жукова(дублёр)
    проспект Мира(дублёр)
    Рязанский проспект(дублёр)'''

    #
    #     Шаблоны проспектов
    #
    patterns = [
        # проспект Пролетарский
        "^(?:\s*)(?:проспект|просп.|просп)(?:\s+)([-ёа-яА-Я0-9 ]+)(?:\s*)$",
        # Пролетарский проспект
        "^(?:\s*)([-ёа-яА-Я0-9 ]+)(?:\s+)(?:проспект|просп.|просп)(?:\s*)$"
    ]

    #     Шаблоны дублёров
    patterns_doubler = [
        # дублёр|(дублёр) Волгоградского проспекта
        "^(?:\s*)(?:дублёр|дублер|дубл.|дубл|дуб.|дуб)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(?:проспект|проспекта|просп.|просп)(?:\s*)$",
        "^(?:\s*)\((?:дублёр|дублер|дубл.|дубл|дуб.|дуб)\)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(?:проспект|проспекта|просп.|просп)(?:\s*)$",
        # дублёр|(дублёр) проспекта Волгоградского
        "^(?:\s*)(?:дублёр|дублер|дубл.|дубл|дуб.|дуб)(?:\s+)(?:проспект|проспекта|просп.|просп)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$",
        "^(?:\s*)\((?:дублёр|дублер|дубл.|дубл|дуб.|дуб)\)(?:\s+)(?:проспект|проспекта|просп.|просп)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$",
        # Волгоградского проспекта дублёр|(дублёр)
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:проспект|проспекта|просп.|просп)(?:\s+)(?:дублёр|дублер|дубл.|дубл|дуб.|дуб)(?:\s*)$",
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:проспект|проспекта|просп.|просп)(?:\s*)\((?:дублёр|дублер|дубл.|дубл|дуб.|дуб)\)(?:\s*)$",
        # проспекта Волгоградского дублёр|(дублёр)
        "^(?:\s*)(?:проспект|проспекта|просп.|просп)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(?:дублёр|дублер|дубл.|дубл|дуб.|дуб)(?:\s*)$",
        "^(?:\s*)(?:проспект|проспекта|просп.|просп)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)\((?:дублёр|дублер|дубл.|дубл|дуб.|дуб)\)(?:\s*)$"
    ]

    patterns_numeral_first = [
        # {numeral} проспект Новогиреево
        "^(?:\s*)(\d{1,2})(?:-й|й)(?:\s+)(?:проспект|просп.|просп)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$",
        # {numeral} Новогиреево проспект
        "^(?:\s*)(\d{1,2})(?:-й|й)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(?:проспект|просп.|просп)(?:\s*)$",
        # проспект {numeral} Новогиреево
        "^(?:\s*)(?:проспект|просп.|просп)(?:\s+)(\d{1,2})(?:-й|й)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$"
    ]

    patterns_numeral_last = [
        # проспект Новогиреево {numeral}
        "^(?:\s*)(?:проспект|просп.|просп)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(\d{1,2})(?:-й|й)(?:\s*)$",
        # Новогиреево проспект {numeral}
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:проспект|просп.|просп)(?:\s+)(\d{1,2})(?:-й|й)(?:\s*)$",
        # Новогиреево {numeral} проспект
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(\d{1,2})(?:-й|й)(?:\s+)(?:проспект|просп.|просп)(?:\s*)$"
    ]

    def __init__(self, string):
        flags = re.IGNORECASE + re.UNICODE

        if not self.is_recognized:
            for pattern in self.patterns_numeral_first:
                m = re.match(pattern, string, flags=flags)
                if m is not None:
                    if m.groups() is not None:
                        #print('patterns_numeral_first')
                        #print(m.groups())
                        if len(m.groups()) > 0:
                            #re.sub(r'\s+', ' ', m.groups()[0].strip())
                            #self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                            self.is_recognized = True
                            self.is_numeral = True
                            self.name = m.groups()[1].strip()
                            self.numeral = m.groups()[0]
                            break
                    else:
                        continue
                else:
                    continue

        if not self.is_recognized:
            for pattern in self.patterns_numeral_last:
                m = re.match(pattern, string, flags=flags)
                if m is not None:
                    if m.groups() is not None:
                        #print('patterns_numeral_last')
                        #print(m.groups())
                        if len(m.groups()) > 0:
                            #re.sub(r'\s+', ' ', m.groups()[0].strip())
                            #self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                            self.is_recognized = True
                            self.is_numeral = True
                            self.name = m.groups()[0].strip()
                            self.numeral = m.groups()[1]
                            break
                    else:
                        continue
                else:
                    continue

        if not self.is_recognized:
            for pattern in self.patterns_doubler:
                m = re.match(pattern, string, flags=flags)
                if m is not None:
                    if m.groups() is not None:
                        #print('patterns_doubler')
                        #print(m.groups())
                        if len(m.groups()) > 0:
                            self.is_recognized = True
                            self.is_simple_name = False
                            self.is_doubler = True
                            self.name = m.groups()[0].strip()
                            break
                    else:
                        continue
                else:
                    continue

        if not self.is_recognized:
            for pattern in self.patterns:
                m = re.match(pattern, string, flags=flags)
                if m is not None:
                    if m.groups() is not None:
                        #print('patterns')
                        #print(m.groups())
                        if len(m.groups()) > 0:
                            re.sub(r'\s+', ' ', m.groups()[0].strip())
                            self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                            self.is_recognized = True
                            break
                    else:
                        continue
                else:
                    continue

    def normalize(self):
        #print(self.name, self.is_recognized, self.is_doubler,self.is_numeral)
        if self.is_recognized:
            if self.is_doubler:
                if self.is_numeral:
                    return self.numeral + '-й ' + self.name + ' проспект (дублёр)'
                else:
                    return 'проспект ' + self.name + ' (дублёр)'
            else:
                if self.is_numeral:
                    return 'проспект ' + self.numeral + '-й ' + self.name
                else:
                    return 'проспект ' + self.name
        else:
            return None