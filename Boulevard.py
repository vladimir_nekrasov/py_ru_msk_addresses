# -*- coding: utf-8 -*-
import re


class Boulevard(object):

    is_recognized = False

    name = None
    numeral = ''

    #бульвар Яна Райниса(дублёр)
    #Кронштадтский бульвар(дублёр)

    #
    #     Шаблоны бульваров
    #
    patterns = [
        # Ореховый бульвар
        "^(?:\s*)(?:бульвар|б-р.|б-р|бульв.|бульв|бул.|бул|бл.|бл)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$",
        # бульвар Ореховый
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:бульвар|б-р.|б-р|бульв.|бульв|бул.|бул|бл.|бл)(?:\s*)$"
    ]

    def __init__(self, string):
        flags = re.IGNORECASE + re.UNICODE

        # Если проезд
        for pattern in self.patterns:
            m = re.match(pattern, string, flags=flags)
            if m is not None:
                if m.groups() is not None:
                    #print(m.groups())
                    if len(m.groups()) > 0:
                        re.sub(r'\s+', ' ', m.groups()[0].strip())
                        self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue

    def normalize(self):
        if self.is_recognized:
            return 'бульвар ' + self.name
        else:
            return None