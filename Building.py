# -*- coding: utf-8 -*-
import re

class Building(object):

    is_recognized = False
    
    is_house = False
    is_building = False
    is_construction = False
    is_house_building = False
    is_house_construction = False
    is_house_building_construction = False
    is_house_construction_building = False
    is_house_building_comment = False

    #
    #     Шаблоны
    #
    #     Для удобочитаемости шаблоны именуются в комментариях, как
    #        rh1 - rule house №1,
    #        rb2 - rule building №2
    #        rс3 - rule construction №2
    #        rсm1 - rule comment №1
    #        rh1-rb2 - rule house №1, building №2
    
    #     Общие шаблоны
    rh1 = "(?:дом.|дом|д.|д|)(?:\s*)(?P<house_number>\d{1,3})(?:\s*)(?P<house_letter>\w{0,1})"
    rh2 = "(?:дом.|дом|д.|д|)(?:\s*)(?P<house_number>\d{1,3}\/\d{1,3})(?:\s*)(?P<house_letter>\w{0,1})"
    rh3 = "(?:дом.|дом|д.|д|)(?:\s*)(?P<house_number>\d{1,3}\\\\\d{1,3})(?:\s*)(?P<house_letter>\w{0,1})"
    rh4 = "(?:дом.|дом|д.|д|)(?:\s*)(?P<house_number>\d{1,3}-\d{1,3})(?:\s*)(?P<house_letter>\w{0,1})"
    
    rb1 = "(?:корпус.|корпус|корп.|корп|к.|к)(?:\s*)(?P<building_number>\d{1,3})(?:\s*)(?P<building_letter>\w{0,1})"
    rb2 = "(?:корпус.|корпус|корп.|корп|к.|к)(?:\s*)(?P<building_number>\d{1,3}-\d{1,3})(?:\s*)(?P<building_letter>\w{0,1})"
    
    rc1 = "(?:строение.|строение|строен.|строен|стр.|стр|ст.|ст|с.|с)(?:\s*)(?P<construction_number>\d{1,3})(?:\s*)(?P<construction_letter>\w{0,1})"
    rc2 = "(?:строение.|строение|строен.|строен|стр.|стр|ст.|ст|с.|с)(?:\s*)(?P<construction_number>\d{1,3}-\d{1,3})(?:\s*)(?P<construction_letter>\w{0,1})"
    
    rcm1 = "(?P<comment>[\w\d\s.,-;\(\)]*)"
    
    '''
    'около вещевого магазина',
    'подьезд 2 код с1к2729 между 2 и 3 этаж',
    'на остановке',
    'под.1,код 148,8 и 9 этаж',
    '(м-н Монетка)',
    '(возле дома)',
    'под.3,9этаж-после 21:00',
    'пд.1',
    'на 8 этаже, в53',
    'на детской площадке',
    'под.2,код150',
    'супермаркет'
    '''
    
    
    #     Шаблоны домов
    house_patterns = [
        # 
        # rh1 дом 1, д.12, д.125А
        "^(?:\s*)" + rh1 + "(?:\s*)$",
        # rh2
        "^(?:\s*)" + rh2 + "(?:\s*)$", 
        # rh3
        "^(?:\s*)" + rh3 + "(?:\s*)$",
        # rh4
        "^(?:\s*)" + rh4 + "(?:\s*)$"
    ]
    
    #     Шаблоны корпусов
    building_patterns = [
        # rb1
        "^(?:\s*)" + rb1 + "(?:\s*)$",
        # rb2
        "^(?:\s*)" + rb2 + "(?:\s*)$"
    ]
    
    #     Шаблоны строений
    construction_patterns = [
        # rc1
        "^(?:\s*)" + rc1 + "(?:\s*)$",
        # rc2
        "^(?:\s*)" + rc2 + "(?:\s*)$"
    ]
    
    #
    #   Шаблоны сборок
    #
    # дом-корпус
    houses_buildings_patterns = [
        # rh1-rb1
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh1-rb2
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",

        # rh2-rb1
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh2-rb2
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",

        # rh3-rb1
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh3-rb2
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",
        
        # rh4-rb1
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh4-rb2
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$"
    ]
    
    # дом-строение
    houses_construction_patterns = [
        # rh1-rc1
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh1-rc2
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",

        # rh2-rc1
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh2-rc2
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",
        
        # rh3-rc1
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh3-rc2
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",

        # rh4-rc1
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh4-rc2
        "^(?:\s*)" + rh4+ "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$"
    ]
    
    # дом - корпус - строение
    houses_buildings_constructions_patterns = [
        # rh1-rb1-rc1
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh1-rb1-rc2
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",
        
        # rh1-rb2-rc1
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh1-rb2-rc2
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",

        # rh2-rb1-rc1
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh2-rb1-rc2
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",

        # rh2-rb2-rc1
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh2-rb2-rc2
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",
        
        # rh3-rb1-rc1
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh3-rb1-rc2
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",

        # rh3-rb2-rc1
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh3-rb2-rc2
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",

        # rh4-rb1-rc1
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh4-rb1-rc2
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$",

        # rh4-rb2-rc1
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)$",
        # rh4-rb2-rc2
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)$"
    ]

    # дом - строение - корпус
    houses_constructions_buildings_patterns = [
        # rh1-rc1-rb1
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh1-rc1-rb2
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",
        
        # rh1-rc2-rb1
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh1-rc2-rb2
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",

        # rh2-rc1-rb1
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh2-rc1-rb2
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",
        
        # rh2-rc2-rb1
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh2-rc2-rb2
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",
        
        # rh3-rc1-rb1
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh3-rc1-rb2
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",
        
        # rh3-rc2-rb1
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh3-rc2-rb2
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",
        
        # rh4-rc1-rb1
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh4-rc1-rb2
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rc1 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$",
        
        # rh4-rc2-rb1
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)$",
        # rh4-rc2-rb2
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rc2 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)$"
    ]
    
    # дом - корпус - комментарий
    houses_buildings_comments_patterns = [
        # rh1-rb1-rcm1
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rcm1 + "(?:\s*)$",
        # rh1-rb2-rcm1
        "^(?:\s*)" + rh1 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rcm1 + "(?:\s*)$",

        # rh2-rb1-rcm1
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rcm1 + "(?:\s*)$",
        # rh2-rb2-rcm1
        "^(?:\s*)" + rh2 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rcm1 + "(?:\s*)$",

        # rh3-rb1-rcm1
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rcm1 + "(?:\s*)$",
        # rh3-rb2-rcm1
        "^(?:\s*)" + rh3 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rcm1 + "(?:\s*)$",
        
        # rh4-rb1-rcm1
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rb1 + "(?:\s*)[,;.]*(?:\s*)" + rcm1 + "(?:\s*)$",
        # rh4-rb2-rcm1
        "^(?:\s*)" + rh4 + "(?:\s*)[,;.]*(?:\s*)" + rb2 + "(?:\s*)[,;.]*(?:\s*)" + rcm1 + "(?:\s*)$"
    ]

    # дом - строение - комментарий
    houses_constructions_comments_patterns = [
        # rh1-rc1-rcm
        
        # rh1-rc2-rcm

        # rh2-rc1-rcm
        
        # rh2-rc2-rcm
        
        # rh3-rc1-rcm
        
        # rh3-rc2-rcm
        
        # rh4-rc1-rcm
        
        # rh4-rc2-rcm
    ]
    
    # дом - корпус - строение - комментарий
    houses_buildings_constructions_comments_patterns = [
        # rh1-rb1-rc1-rcm
        
        # rh1-rb1-rc2-rcm
        
        # rh1-rb2-rc1-rcm
        
        # rh1-rb2-rc2-rcm

        # rh2-rb1-rc1-rcm
        
        # rh2-rb1-rc2-rcm

        # rh2-rb2-rc1-rcm
        
        # rh2-rb2-rc2-rcm
        
        # rh3-rb1-rc1-rcm
        
        # rh3-rb1-rc2-rcm

        # rh3-rb2-rc1-rcm
        
        # rh3-rb2-rc2-rcm

        # rh4-rb1-rc1-rcm
        
        # rh4-rb1-rc2-rcm

        # rh4-rb2-rc1-rcm
        
        # rh4-rb2-rc2-rcm
    ]

    # дом - строение - корпус - комментарий
    houses_constructions_buildings_comments_patterns = [
        # rh1-rc1-rb1-rcm
        
        # rh1-rc1-rb2-rcm
        
        # rh1-rc2-rb1-rcm
        
        # rh1-rc2-rb2-rcm

        # rh2-rc1-rb1-rcm
        
        # rh2-rc1-rb2-rcm
        
        # rh2-rc2-rb1-rcm
        
        # rh2-rc2-rb2-rcm
        
        # rh3-rc1-rb1-rcm
        
        # rh3-rc1-rb2-rcm
        
        # rh3-rc2-rb1-rcm
        
        # rh3-rc2-rb2-rcm
        
        # rh4-rc1-rb1-rcm
        
        # rh4-rc1-rb2-rcm
        
        # rh4-rc2-rb1-rcm
        
        # rh4-rc2-rb2-rcm
    ]
    
    


    def __init__(self, string):
        flags = re.IGNORECASE + re.UNICODE

        for pattern in self.house_patterns:
            m = re.match(pattern, string, flags=flags)
            if m is not None:
                #print(m.group('number'), m.group('letter'))
                if m.groups() is not None:
                    if m.group('house_number') is not None:
                        self.number = m.group('house_number')
                        self.letter = m.group('house_letter')
                        self.is_house = True
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue
                
        for pattern in self.building_patterns:
            m = re.match(pattern, string, flags=flags)
            if m is not None:
                #print(m.group('building_number'), m.group('building_letter'))
                if m.groups() is not None:
                    if m.group('building_number') is not None:
                        self.number = m.group('building_number')
                        self.letter = m.group('building_letter')
                        self.is_building = True
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue

        for pattern in self.construction_patterns:
            m = re.match(pattern, string, flags=flags)
            if m is not None:
                #print(m.group('construction_number'), m.group('construction_letter'))
                if m.groups() is not None:
                    if m.group('construction_number') is not None:
                        self.number = m.group('construction_number')
                        self.letter = m.group('construction_letter')
                        self.is_construction = True
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue
                
        for pattern in self.houses_buildings_patterns:
            m = re.match(pattern, string, flags=flags)
            print(pattern, string)
            if m is not None:
                print(m.groupdict().keys(), 'construction_number' not in m.groupdict().keys())
                if m.groups() is not None:
                    if (m.group('house_number') is not None) and (m.group('building_number') is not None) and ('construction_number' not in m.groupdict().keys()):
                        self.house_number = m.group('house_number')
                        self.house_letter = m.group('house_letter')
                        self.building_number = m.group('building_number')
                        self.building_letter = m.group('building_letter')
                        self.is_house_building = True
                        self.is_recognized = True
                        #print(pattern)
                        break
                else:
                    continue
            else:
                continue
                
        for pattern in self.houses_construction_patterns:
            m = re.match(pattern, string, flags=flags)
            if m is not None:
                #print(m.groupdict().keys(), 'building_number' not in m.groupdict().keys())
                if m.groups() is not None:
                    if (m.group('house_number') is not None) and (m.group('construction_number') is not None) and ('building_number' not in m.groupdict().keys()):
                        self.house_number = m.group('house_number')
                        self.house_letter = m.group('house_letter')
                        self.construction_number = m.group('construction_number')
                        self.construction_letter = m.group('construction_letter')
                        self.is_house_construction = True
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue
                
        for pattern in self.houses_buildings_constructions_patterns:
            m = re.match(pattern, string, flags=flags)
            if m is not None:
                #print(m.groups())
                if m.groups() is not None:
                    if (m.group('house_number') is not None) and (m.group('building_number') is not None) and (m.group('construction_number') is not None):
                        self.house_number = m.group('house_number')
                        self.house_letter = m.group('house_letter')
                        self.building_number = m.group('building_number')
                        self.building_letter = m.group('building_letter')
                        self.construction_number = m.group('construction_number')
                        self.construction_letter = m.group('construction_letter')
                        self.is_house_building_construction = True
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue
                
        for pattern in self.houses_constructions_buildings_patterns:
            m = re.match(pattern, string, flags=flags)
            if m is not None:
                #print(m.groups())
                if m.groups() is not None:
                    if (m.group('house_number') is not None) and (m.group('building_number') is not None) and (m.group('construction_number') is not None):
                        self.house_number = m.group('house_number')
                        self.house_letter = m.group('house_letter')
                        self.building_number = m.group('building_number')
                        self.building_letter = m.group('building_letter')
                        self.construction_number = m.group('construction_number')
                        self.construction_letter = m.group('construction_letter')
                        self.is_house_construction_building = True
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue
                
        for pattern in self.houses_buildings_comments_patterns:
            m = re.match(pattern, string, flags=flags)
            if m is not None:
                #print(m.groups())
                if m.groups() is not None:
                    if (m.group('house_number') is not None) and (m.group('building_number') is not None):
                        self.house_number = m.group('house_number')
                        self.house_letter = m.group('house_letter')
                        self.building_number = m.group('building_number')
                        self.building_letter = m.group('building_letter')
                        self.is_house_building_comment = True
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue
                
                
        
                
                
                
    def normalize(self):
        if self.is_recognized:
            if self.is_house:
                if self.letter is not None:
                    return 'дом ' + self.number + self.letter
                else:
                    return 'дом ' + self.number

            elif self.is_building:
                if self.letter is not None:
                    return 'корпус ' + self.number + self.letter
                else:
                    return 'корпус ' + self.number

            elif self.is_construction:
                if self.letter is not None:
                    return 'строение ' + self.number + self.letter
                else:
                    return 'строение ' + self.number

            elif self.is_house_building:
                if self.house_letter is not None:
                    if self.building_letter is not None:
                        return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + self.building_letter
                    else:
                        return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number
                else:
                    if self.building_letter is not None:
                        return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + self.building_letter
                    else:
                        return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number
                        
            elif self.is_house_construction:
                if self.house_letter is not None:
                    if self.construction_letter is not None:
                        return 'дом ' + self.house_number + self.house_letter + ', ' + 'строение ' + self.construction_number + self.construction_letter
                    else:
                        return 'дом ' + self.house_number + self.house_letter + ', ' + 'строение' + self.construction_number
                else:
                    if self.construction_letter is not None:
                        return 'дом ' + self.house_number + ', ' + 'строение ' + self.construction_number + self.construction_letter
                    else:
                        return 'дом ' + self.house_number + ', ' + 'строение ' + self.construction_number
                        
            elif self.is_house_building_construction:
                if self.house_letter is not None:
                    if self.building_letter is not None:
                        if self.construction_letter is not None:
                            return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + self.building_letter + ', ' + 'строение ' + self.construction_number + self.construction_letter
                        else:
                            return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + self.building_letter + ', ' + 'строение ' + self.construction_number
                    else:
                        if self.construction_letter is not None:
                            return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + ', ' + 'строение ' + self.construction_number + self.construction_letter
                        else:
                            return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + ', ' + 'строение ' + self.construction_number
                else:
                    if self.building_letter is not None:
                        if self.construction_letter is not None:
                            return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + self.building_letter + ', ' + 'строение ' + self.construction_number + self.construction_letter
                        else:
                            return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + self.building_letter + ', ' + 'строение ' + self.construction_number
                    else:
                        if self.construction_letter is not None:
                            return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + ', ' + 'строение ' + self.construction_number + self.construction_letter
                        else:
                            return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + ', ' + 'строение ' + self.construction_number


            # дом-строение-корпус отображается, так же, как дом-корпус-строение
            elif self.is_house_construction_building:
                if self.house_letter is not None:
                    if self.building_letter is not None:
                        if self.construction_letter is not None:
                            return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + self.building_letter + ', ' + 'строение ' + self.construction_number + self.construction_letter
                        else:
                            return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + self.building_letter + ', ' + 'строение ' + self.construction_number
                    else:
                        if self.construction_letter is not None:
                            return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + ', ' + 'строение ' + self.construction_number + self.construction_letter
                        else:
                            return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + ', ' + 'строение ' + self.construction_number
                else:
                    if self.building_letter is not None:
                        if self.construction_letter is not None:
                            return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + self.building_letter + ', ' + 'строение ' + self.construction_number + self.construction_letter
                        else:
                            return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + self.building_letter + ', ' + 'строение ' + self.construction_number
                    else:
                        if self.construction_letter is not None:
                            return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + ', ' + 'строение ' + self.construction_number + self.construction_letter
                        else:
                            return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + ', ' + 'строение ' + self.construction_number
                        
            # комментарий не выводится
            elif self.is_house_building_comment:
                if self.house_letter is not None:
                    if self.building_letter is not None:
                        return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number + self.building_letter
                    else:
                        return 'дом ' + self.house_number + self.house_letter + ', ' + 'корпус ' + self.building_number
                else:
                    if self.building_letter is not None:
                        return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number + self.building_letter
                    else:
                        return 'дом ' + self.house_number + ', ' + 'корпус ' + self.building_number
                        
                        
        else:
            return None