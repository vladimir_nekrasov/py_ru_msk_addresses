# -*- coding: utf-8 -*-
import re


class Driveway(object):

    is_recognized = False

    is_simple_name = False
    is_numeral_name = False

    name = None
    numeral = ''

    #проезд 6535
    #Проезд N65
    #проезд №3
    #проезд №4745
    #проектируемый проезд 6260
    #Проектируемый проезд № 3538
    #Центральный проезд Хорошёвского Серебряного Бора

    ###
    ###     Шаблоны переулков
    ###
    patterns = [
        # проезд Загорьевский
        "^(?:\s*)(?:проезд|пр.|пр)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$",
        # Загорьевский проезд
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:проезд|пр.|пр)(?:\s*)$"
    ]

    ###     Шаблоны переулков с числительным до названия
    patterns_number_first = [
        # 10-й|10й|10 проезд Полевой
        "^(?:\s*)([\d]+)(?:-й|й|)(?:\s+)(?:проезд|пр.|пр)([-ёа-яА-Я ]+)(?:\s*)$",
        # (10-й|10й|10) проезд Полевой
        "^(?:\s*)\(([\d]+)(?:-й|й|)\)(?:\s+)(?:проезд|пр.|пр)([-ёа-яА-Я ]+)(?:\s*)$",
        # проезд 10-й|10й|10 Полевой
        "^(?:\s*)(?:проезд|пр.|пр)(?:\s+)(\d{1,2})(?:-й|й|)([-ёа-яА-Я ]+)(?:\s*)$",
        # проезд (10-й|10й|10) Полевой
        "^(?:\s*)(?:проезд|пр.|пр)(?:\s+)\((\d{1,2})(?:-й|й|)\)([-ёа-яА-Я ]+)(?:\s*)$",
        # 10-й|10й|10 Полевой проезд
        "^(?:\s*)(\d{1,2})(?:-й|й|)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(?:проезд|пр.|пр)(?:\s*)$",
        # (10-й|10й|10) Полевой проезд
        "^(?:\s*)\((\d{1,2})(?:-й|й|)\)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(?:проезд|пр.|пр)(?:\s*)$",
    ]

    ###     Шаблоны переулков с числительным после названия
    patterns_number_last = [
        # Полевой 10-й|10й|10 проезд
        "^(?:\s*)([-ёа-яА-Я ]+)(\d{1,2})(?:-й|й|)(?:\s+)(?:проезд|пр.|пр)(?:\s*)$",
        # Полевой (10-й|10й|10) проезд
        "^(?:\s*)([-ёа-яА-Я ]+)\((\d{1,2})(?:-й|й|)\)(?:\s+)(?:проезд|пр.|пр)(?:\s*)$",

        # Полевой проезд 10-й|10й|10
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:проезд|пр.|пр)(?:\s+)(\d{1,2})(?:-й|й|)(?:\s*)$",
        # Полевой проезд (10-й|10й|10)
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:проезд|пр.|пр)(?:\s+)\((\d{1,2})(?:-й|й|)\)(?:\s*)$",

        # проезд Полевой 10-й|10й|10
        "^(?:\s*)(?:проезд|пр.|пр)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(\d{1,2})(?:-й|й|)(?:\s*)$",
        # проезд Полевой (10-й|10й|10)
        "^(?:\s*)(?:проезд|пр.|пр)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)\((\d{1,2})(?:-й|й|)\)(?:\s*)$"
        # проезд Полевой 10-й|10й|10
        "^(?:\s*)(?:проезд|пр.|пр)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(\d{1,2})(?:-й|й|)(?:\s*)$",
        # проезд Полевой (10-й|10й|10)
        "^(?:\s*)(?:проезд|пр.|пр)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)\((\d{1,2})(?:-й|й|)\)(?:\s*)$"
    ]


    def __init__(self, string):
        flags = re.IGNORECASE + re.UNICODE

        # Если проезд
        for pattern in self.patterns:
            m = re.match(pattern, string, flags=flags)
            if m != None:
                if m.groups() != None:
                    #print(m.groups())
                    if len(m.groups()) > 0:
                        re.sub(r'\s+', ' ', m.groups()[0].strip())
                        self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                        self.is_recognized = True
                        self.is_simple_name = True
                        self.is_numeral_name = False
                        break
                else:
                    continue
            else:
                continue

        if not self.is_recognized:
            for pattern in self.patterns_number_first:
                m = re.match(pattern, string, flags=flags)
                if m is not None:
                    if m.groups() is not None:
                        #print(m.groups())
                        if len(m.groups()) > 0:
                            self.is_recognized = True
                            self.is_simple_name = False
                            self.is_numeral_name = True
                            self.name = re.sub(r'\s+', ' ', m.groups()[1].strip())
                            self.numeral = m.groups()[0]
                            break
                    else:
                        continue
                else:
                    continue
        if not self.is_recognized:
            for pattern in self.patterns_number_last:
                m = re.match(pattern, string, flags=flags)
                if m is not None:
                    if m.groups() is not None:
                        #print(m.groups())
                        if len(m.groups()) > 0:
                            self.is_recognized = True
                            self.is_simple_name = False
                            self.is_numeral_name = True
                            self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                            self.numeral = m.groups()[1]
                            break
                    else:
                        continue
                else:
                    continue

    def normalize(self):
        if self.is_recognized:
            if self.is_simple_name:
                return 'проезд ' + self.name
            elif self.is_numeral_name:
                return 'проезд ' + self.numeral + '-й ' + self.name
        else:
            return None