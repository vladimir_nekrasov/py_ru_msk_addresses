# -*- coding: utf-8 -*-
import re


class Promenade(object):

    is_recognized = False

    name = None
    numeral = ''

    #
    #     Шаблоны набережных
    #
    patterns = [
        # набережная Котельническая
        "^(?:\s*)(?:набережная|набер.|набер|наб.|наб)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$",
        # Котельническая набережная
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:набережная|набер.|набер|наб.|наб)(?:\s*)$"
    ]

    def __init__(self, string):
        flags = re.IGNORECASE + re.UNICODE

        for pattern in self.patterns:
            m = re.match(pattern, string, flags=flags)

            if m is not None:
                if m.groups() is not None:
                    #print(m.groups())
                    if len(m.groups()) > 0:
                        self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue

    def normalize(self):
        if self.is_recognized:
            return 'набережная ' + self.name
        else:
            return None