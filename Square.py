# -*- coding: utf-8 -*-
import re
from .Station import stations_dict, near_the_stations_squares

class Square(object):

    is_recognized = False
    is_near_the_station = False

    name = None
    numeral = ''

    #площадь 60 - летия СССР

    #
    #     Шаблоны бульваров
    #
    patterns = [
        # Красная площадь
        "^(?:\s*)(?:площадь|пл.|пл)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$",
        # площадь Красная
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:площадь|пл.|пл)(?:\s*)$"
    ]
    
    station_patterns = [
        # Курского вокзала
        "^(?:\s*)(?:вокзала|вокзал|вкз.|вкз)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$",
        # вокзала Курского
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:вокзала|вокзал|вкз.|вкз)(?:\s*)$"
    ]
    
    def __init__(self, string):
        self.stations_dict = stations_dict
        self.near_the_stations_squares = near_the_stations_squares
        flags = re.IGNORECASE + re.UNICODE
        
        # Если площадь
        for pattern in self.patterns:
            m = re.match(pattern, string, flags=flags)
            if m is not None:
                if m.groups() is not None:
                    #print(m.groups())
                    if len(m.groups()) > 0:
                        #re.sub(r'\s+', ' ', m.groups()[0].strip())
                        self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                        #print('name:', self.name)
                        if self.name in self.near_the_stations_squares:
                            self.is_near_the_station = True
                            self.station = self.near_the_stations_squares[self.name] + ' вокзал'
                        else:
                            #print(self.name)
                            # Является ли площадь привокзальной
                            for pattern1 in self.station_patterns:
                                m1 = re.match(pattern1, self.name, flags=flags)
                                if m1 is not None:
                                    if m1.groups() is not None:
                                        #print(m1.groups())
                                        if len(m1.groups()) > 0:
                                            station_word = m1.groups()[0].strip()
                                            #print('station_word:', station_word)
                                            if station_word in self.stations_dict:
                                                self.is_near_the_station = True
                                                self.station = self.stations_dict[station_word] + ' вокзал'
                                                #print('station:', self.station)
                        
                        
                        #print(self.name)
                        self.is_recognized = True
                        break
                else:
                    continue
            else:
                continue

    def normalize(self):
        if self.is_recognized:
            return 'площадь ' + self.name
        else:
            return None