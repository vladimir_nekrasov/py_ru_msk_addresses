# -*- coding: utf-8 -*-
import re


class Street(object):

    is_recognized = False
    is_simple_name = False
    is_numeral_name = False

    name = None
    numeral = ''


    #'1905 года улица'
    #'1905 Года улица'
    #'1905 Года улица'
    #'улица 1905 года'

    #'1812 года улица'
    #'1812 Года улица'
    #'улица 1812 Года'

    #'улица 800 - летия Москвы'

    #1-я улица 8 Марта
    #4-я улица 8 Марта
    #Кантемировская улица(дублёр)
    #Покровская улица (временный объезд стройплощадки)

    #улица Героев Панфиловцев(дублёр)
    #улица И.Ильинского
    #улица К.Маркса
    #улица Корнейчука(дублёр)
    #улица Мичуринский Проспект, Олимпийская Деревня
    #улица Л.Орловой

    #
    #     Шаблоны улиц
    #
    patterns = [
        # улица Ширшова
        "^(?:\s*)(?:улица|ул.|ул)(?:\s+)([-ёа-яА-Я ]+)(?:\s*)$",
        # Ширшова улица
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:улица|ул.|ул)(?:\s*)$"
    ]

    #     Шаблоны улиц с числительным до названия улицы
    patterns_number_first = [
        # 10-я|10я|10 улица Соколиной горы
        "^(?:\s*)([\d]+)(?:-я|я|)(?:\s+)(?:улица|ул.|ул)([-ёа-яА-Я ]+)(?:\s*)$",
        # (10-я|10я|10) улица Соколиной горы
        "^(?:\s*)\(([\d]+)(?:-я|я|)\)(?:\s+)(?:улица|ул.|ул)([-ёа-яА-Я ]+)(?:\s*)$",
        # улица 10-я|10я|10 Соколиной горы
        "^(?:\s*)(?:улица|ул.|ул)(?:\s+)(\d{1,2})(?:-я|я|)([-ёа-яА-Я ]+)(?:\s*)$",
        # улица (10-я|10я|10) Соколиной горы
        "^(?:\s*)(?:улица|ул.|ул)(?:\s+)\((\d{1,2})(?:-я|я|)\)([-ёа-яА-Я ]+)(?:\s*)$",
        # 10-я|10я|10 Соколиной горы улица
        "^(?:\s*)(\d{1,2})(?:-я|я|)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(?:улица|ул.|ул)(?:\s*)$",
        # (10-я|10я|10) Соколиной горы улица
        "^(?:\s*)\((\d{1,2})(?:-я|я|)\)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(?:улица|ул.|ул)(?:\s*)$",
    ]

    #     Шаблоны улиц с числительным после названия улицы
    patterns_number_last = [
        # Соколиной горы 10-я|10я|10 улица
        "^(?:\s*)([-ёа-яА-Я ]+)(\d{1,2})(?:-я|я|)(?:\s+)(?:улица|ул.|ул)(?:\s*)$",
        # Соколиной горы (10-я|10я|10) улица
        "^(?:\s*)([-ёа-яА-Я ]+)\((\d{1,2})(?:-я|я|)\)(?:\s+)(?:улица|ул.|ул)(?:\s*)$",

        # Соколиной горы улица 10-я|10я|10
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:улица|ул.|ул)(?:\s+)(\d{1,2})(?:-я|я|)(?:\s*)$",
        # Соколиной горы улица (10-я|10я|10)
        "^(?:\s*)([-ёа-яА-Я ]+)(?:\s+)(?:улица|ул.|ул)(?:\s+)\((\d{1,2})(?:-я|я|)\)(?:\s*)$",

        # улица Соколиной горы 10-я|10я|10
        "^(?:\s*)(?:улица|ул.|ул)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)(\d{1,2})(?:-я|я|)(?:\s*)$",
        # улица Соколиной горы (10-я|10я|10)
        "^(?:\s*)(?:улица|ул.|ул)(?:\s+)([-ёа-яА-Я ]+)(?:\s+)\((\d{1,2})(?:-я|я|)\)(?:\s*)$"
    ]

    # Шаблоны улиц с номером года
    patterns_year = [
        # 1905 года улица
        "^(?:\s*)(\d{1,4})(?:-го|(?:\s+)-(?:\s+)го|го|(?:\s+)го|)(?:\s+)(?:года|г.|г)(?:\s+)(?:улица|ул.|ул)(?:\s*)$",
        # года 1905 улица
        "^(?:\s*)(?:года|г.|г)(?:\s+)(\d{1,4})(?:-го|(?:\s+)-(?:\s+)го|го|(?:\s+)го|)(?:\s+)(?:улица|ул.|ул)(?:\s*)$",
        # улица 1905 года
        "^(?:\s*)(?:улица|ул.|ул)(?:\s+)(\d{1,4})(?:-го|(?:\s+)-(?:\s+)го|го|(?:\s+)го|)(?:\s+)(?:года|г.|г)(?:\s*)$",
        # улица года 1905
        "^(?:\s*)(?:улица|ул.|ул)(?:\s+)(?:года|г.|г)(?:\s+)(\d{1,4})(?:-го|(?:\s+)-(?:\s+)го|го|(?:\s+)го|)(?:\s*)$"
    ]


    def __init__(self, string):
        flags = re.IGNORECASE + re.UNICODE
        # Если улица
        for pattern in self.patterns:
            m = re.match(pattern, string, flags=flags)
            if m != None:
                if m.groups() != None:
                    #print(m.groups())
                    if len(m.groups()) > 0:
                        self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                        #self.name = m.groups()[0].strip()
                        self.is_recognized = True
                        self.is_simple_name = True
                        self.is_numeral_name = False
                        break
                else:
                    continue
            else:
                continue

        if not self.is_recognized:
            for pattern in self.patterns_year:
                m = re.match(pattern, string, flags=flags)
                if m != None:
                    if m.groups() != None:
                        #print(m.groups())
                        if len(m.groups()) > 0:
                            self.is_recognized = True
                            self.is_simple_name = False
                            self.is_year_name = True
                            self.year = m.groups()[0].strip()
                            break
                    else:
                        continue
                else:
                    continue

        if not self.is_recognized:
            for pattern in self.patterns_number_first:
                m = re.match(pattern, string, flags=flags)
                if m != None:
                    if m.groups() != None:
                        #print(m.groups())
                        if len(m.groups()) > 0:
                            self.is_recognized = True
                            self.is_simple_name = False
                            self.is_numeral_name = True
                            #self.name = m.groups()[1].strip()
                            self.name = re.sub(r'\s+', ' ', m.groups()[1].strip())
                            self.numeral = m.groups()[0]
                            break
                    else:
                        continue
                else:
                    continue
        if not self.is_recognized:
            for pattern in self.patterns_number_last:
                m = re.match(pattern, string, flags=flags)
                if m != None:
                    if m.groups() != None:
                        #print(m.groups())
                        if len(m.groups()) > 0:
                            self.is_recognized = True
                            self.is_simple_name = False
                            self.is_numeral_name = True
                            #self.name = m.groups()[0].strip()
                            self.name = re.sub(r'\s+', ' ', m.groups()[0].strip())
                            self.numeral = m.groups()[1]
                            break
                    else:
                        continue
                else:
                    continue

    def normalize(self):
        if self.is_recognized:
            if self.is_simple_name:
                return 'улица ' + self.name
            elif self.is_numeral_name:
                return 'улица ' + self.numeral + '-я ' + self.name
            elif self.is_year_name:
                return 'улица ' + self.year + ' года'
        else:
            return None