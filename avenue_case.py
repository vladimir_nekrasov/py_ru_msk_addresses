# -*- coding: utf-8 -*-
import unittest
from AddressAbstractCase import AbstractAddressTest
from Address import Address

'''
Тесты проспектов
'''


class AvenueAddressTest(AbstractAddressTest):
    subst_word_1 = 'проспект'
    subst_word_2 = 'проспекта'
    subst_word_3 = 'дублёр'

    one_digit_numerals = [
        '2-й',
        '2й',
    ]

    two_digit_numerals = [
        '20-й',
        '20й'
    ]

    abbrev1 = [
        'просп.',
        'просп'
    ]

    abbrev2 = [
        'дублер',
        'дубл.',
        'дубл',
        'дуб.',
        'дуб'
    ]

    test1_src_strings = [
        'проспект Пролетарский',
        'Пролетарский проспект'
    ]

    test2_src_strings = [
        '60-летия Октября проспект',
        'проспект 60-летия Октября'
    ]

    test3_src_strings = [
        '{numeral} проспект Новогиреево',
        '{numeral} Новогиреево проспект',
        'проспект {numeral} Новогиреево',
        'проспект Новогиреево {numeral}',
        'Новогиреево {numeral} проспект',
        'Новогиреево проспект {numeral}'
    ]

    test4_src_strings = [
        'Волгоградский проспект дублёр',
        'дублёр Волгоградский проспект',
        'дублёр проспект Волгоградский',
        'проспект Волгоградский дублёр'
    ]

    #
    # Формат "проспект Пролетарский"
    #
    def test1(self):
        src = self.test1_src_strings
        test = "проспект Пролетарский"

        print('Тест \"' + test + '\"')

        test_strings = []

        # замена сокращений
        for item in src:
            test_strings.append(item)
            test_strings.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "проспект 60-летия Октября"
    #
    def test2(self):
        src = self.test2_src_strings
        test = "проспект 60-летия Октября"

        print('Тест \"' + test + '\"')

        test_strings = []
        test_strings.extend(src)

        # замена сокращений
        tmp = []
        for item in src:
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))
        test_strings.extend(tmp)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)


    def test3(self):
        src = self.test3_src_strings
        test = "проспект 2-й Новогиреево"

        print('Тест \"' + test + '\"')

        test_strings = []

        # подстановка числительных
        tmp = []
        for item in src:
            tmp.extend(self.substitute_numerals(item, self.one_digit_numerals))
        test_strings.extend(tmp)

        # замена сокращений 'шоссе'
        tmp = []
        for item in test_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    def test4(self):
        src = self.test3_src_strings
        test = "проспект 20-й Новогиреево"

        print('Тест \"' + test + '\"')

        test_strings = []

        # подстановка числительных
        tmp = []
        for item in src:
            tmp.extend(self.substitute_numerals(item, self.two_digit_numerals))
        test_strings.extend(tmp)

        # замена сокращений 'шоссе'
        tmp = []
        for item in test_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "проспект Волгоградский (дублёр)"
    #
    def test5(self):
        src = self.test4_src_strings
        test = "проспект Волгоградский (дублёр)"

        print('Тест \"' + test + '\"')

        test_strings = []

        # замена сокращений 'проспект'
        tmp = []
        for item in src:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))
        test_strings.extend(tmp)

        # замена сокращений 'проспекта'
        tmp = []
        for item in test_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_2, self.abbrev1))
        test_strings.extend(tmp)

        # замена сокращений 'дублёр'
        for item in tmp:
            test_strings.append(item)
            test_strings.append(self.brackets(item, self.subst_word_3))
            test_strings.extend(self.abbreviations(item, self.subst_word_3, self.abbrev2, with_brackets=True))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()

