# -*- coding: utf-8 -*-
import unittest
from AddressAbstractCase import AbstractAddressTest
from Address import Address

'''
Тесты переулков
'''


class BoulevardAddressTest(AbstractAddressTest):
    subst_word = 'бульвар'

    abbrev = [
        'б-р.',
        'б-р',
        'бульв.',
        'бульв',
        'бул.',
        'бул',
        'бл.',
        'бл'
    ]

    test1_src_strings = [
        'Ореховый бульвар',
        'бульвар Ореховый'
    ]

    test2_src_strings = [
        'Маршала Рокоссовского бульвар',
        'бульвар Маршала Рокоссовского'
    ]

    #
    # Формат "бульвар Ореховый"
    #
    def test1(self):
        src = self.test1_src_strings
        test = "бульвар Ореховый"

        print('Тест \"' + test + '\"')

        test_strings = []

        # замена сокращений
        for item in src:
            test_strings.append(item)
            test_strings.extend(self.abbreviations(item))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "бульвар Маршала Рокоссовского"
    #
    def test2(self):
        src = self.test2_src_strings
        test = "бульвар Маршала Рокоссовского"

        print('Тест \"' + test + '\"')

        test_strings = []
        test_strings.extend(src)

        # замена сокращений
        tmp = []
        for item in src:
            tmp.extend(self.abbreviations(item))
        test_strings.extend(tmp)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()

