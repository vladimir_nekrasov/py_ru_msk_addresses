# -*- coding: utf-8 -*-
import unittest
from Building import Building

'''
Тесты распознавания строк номеров зданий
'''
class BuildingStringTest(unittest.TestCase):
    '''
    рассматриваются комбинации:
    дом
    корпус
    строение
    дом - корпус
    дом - строение
    дом - корпус - строение
    дом - строение - корпус
    дом - корпус - комментарий
    дом - строение - комментарий
    дом - корпус - строение - комментарий
    дом - строение - корпус - комментарий
    '''
    
    houses_strings = ['', 'д', 'дом']
    
    buildings_strings = ['к', 'корп', 'корпус']
    
    constructions_strings = ['с', 'ст', 'стр', 'строен', 'строение']
    
    constrictions = ['', '.']
    
    spaces = [' ']
    
    dividers = [' ', ',']
    
    houses_numbers = {
        '1': '1',
        '12': '12',
        '125': '125',
        '125А': '125А',
        '125 А': '125А',
        '12/2': '12/2',
        '12\\2': '12\\2',
        '12-2': '12-2',
        '12-2А': '12-2А',
        '12-2 А': '12-2А'
    }

    buildings_numbers = {
        '1': '1',
        '12': '12',
        '125': '125',
        '5-6': '5-6'
    }
    
    construction_numbers = {
        '1': '1',
        '12': '12',
        '125': '125',
        '5-6': '5-6'
    }
    
    comments = [
        'около вещевого магазина',
        'подьезд 2 код с1к2729 между 2 и 3 этаж',
        'на остановке',
        'под.1,код 148,8 и 9 этаж',
        '(м-н Монетка)',
        '(возле дома)',
        'под.3,9этаж-после 21:00',
        'пд.1',
        'на 8 этаже, в53',
        'на детской площадке',
        'под.2,код150',
        'супермаркет'
    ]
    
    # тестовые строки домов, корпусов, строений
    house_test_strings = None
    building_test_strings = None
    construction_test_strings = None
    
    # тестовые строки комбинаций
    # варианты всегда начинаются с дома
    #дом - корпус
    houses_buildings = None
    #дом - строение
    houses_constructions = None
    #дом - корпус - строение
    houses_buildings_constructions = None
    #дом - строение - корпус
    houses_constructions_buildings = None
    #дом - корпус - комментарий
    houses_buildings_comments = None
    #дом - строение - комментарий
    houses_constructions_comments = None
    #дом - корпус - строение - комментарий
    houses_buildings_constructions_comments = None
    #дом - строение - корпус - комментарий
    houses_constructions_buildings_comments = None
    
    is_initialized = False
    

    def setUp(self):
        print("setup")
        
        self.house_test_strings = {}
        self.building_test_strings = {}
        self.construction_test_strings = {}
        
        if not self.is_initialized:
            # сборка тестовых строк домов
            for house_string in self.houses_strings:
                for house_number, house_number_value in self.houses_numbers.items():
                    for constriction in self.constrictions:
                        for space in self.spaces:
                            if (house_string == '') and (constriction == '.'):
                                continue
                            if (house_string == ''):
                                key = house_number
                                self.house_test_strings[key] = 'дом ' + house_number_value
                            else:
                                key = house_string + constriction + space + house_number
                                self.house_test_strings[key] = 'дом ' + house_number_value

            # сборка тестовых строк корпусов
            for building_string in self.buildings_strings:
                for building_number, building_number_value in self.buildings_numbers.items():
                    for constriction in self.constrictions:
                        for space in self.spaces:
                            if (building_string == ''):
                                key = building_number
                                self.building_test_strings[key] = 'корпус ' + building_number_value
                            else:
                                key = building_string + constriction + space + building_number
                                self.building_test_strings[key] = 'корпус ' + building_number_value

            # сборка тестовых строк строений
            for construction_string in self.constructions_strings:
                for construction_number, construction_number_value in self.construction_numbers.items():
                    for constriction in self.constrictions:
                        for space in self.spaces:
                            if (construction_string == ''):
                                key = construction_number
                                self.construction_test_strings[key] = 'строение ' + construction_number_value
                            else:
                                key = construction_string + constriction + space + construction_number
                                self.construction_test_strings[key] = 'строение ' + construction_number_value

            self.is_initialized = True
            


    def test_houses(self):
        print("Тест дом")
        result = True
        # дома
        for key, value in self.house_test_strings.items():
            sn = Building(key)
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != value:
                    result = False
                    break
            #print(key, sn.normalize(), value, result)

        self.assertTrue(result)
        

    def test_buildings(self):
        print("Тест корпус")
        result = True
        for key, value in self.building_test_strings.items():
            sn = Building(key)
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != value:
                    result = False
                    break

            #print(key, sn.normalize(), value, result)
        self.assertTrue(result)

    def test_constructions(self):
        print("Тест строение")
        result = True
        for key, value in self.construction_test_strings.items():
            sn = Building(key)
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != value:
                    result = False
                    break

            #print(key, sn.normalize(), value, result)
        self.assertTrue(result)


    def test_houses_buildings(self):
        print("Тест дом-корпус")
        self.houses_buildings = {}

        # дом - корпус
        for house_string_key, house_string_value in self.house_test_strings.items():
            for building_string_key, building_string_value in self.building_test_strings.items():
                for divider in self.dividers:
                    for space in self.spaces:
                        key = house_string_key + divider + space + building_string_key
                        value = house_string_value + ', ' + building_string_value
                        self.houses_buildings[key] = value
                     
        result = True
        for key, value in self.houses_buildings.items():
            sn = Building(key)
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != value:
                    result = False
                    break
            #print('*' + key + '*', '*' + sn.normalize() + '*', '*' + value + '*', result)
        self.assertTrue(result)


    def test_houses_constructions(self):
        print("Тест дом-строение")
        self.houses_constructions = {}

        for house_string_key, house_string_value in self.house_test_strings.items():
            for construction_string_key, construction_string_value in self.construction_test_strings.items():
                for divider in self.dividers:
                    for space in self.spaces:
                        key = house_string_key + divider + space + construction_string_key
                        value = house_string_value + ', ' + construction_string_value
                        self.houses_constructions[key] = value
        
        result = True
        for key, value in self.houses_constructions.items():
            sn = Building(key)
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != value:
                    result = False
                    break

            #if not result:
            #print('*' + key + '*', '*' + sn.normalize() + '*', '*' + value + '*', result)
        self.assertTrue(result)


    def test_houses_buildings_constructions(self):
        print("Тест дом-корпус-строение")
        self.houses_buildings_constructions = {}
        
        for house_string_key, house_string_value in self.house_test_strings.items():
            # два одинаковых цикла по разделителям и пробелам
            for divider1 in self.dividers:
                for space1 in self.spaces:
                    if (divider1 == ' ') and (space1 == ' '):
                        divider1 = ''
                    for building_string_key, building_string_value in self.building_test_strings.items():
                        for construction_string_key, construction_string_value in self.construction_test_strings.items():
                            # два одинаковых цикла по разделителям и пробелам
                            for divider2 in self.dividers:
                                for space2 in self.spaces:
                                    if (divider2 == ' ') and (space2 == ' '):
                                        divider2 = ''
                                    key = house_string_key + divider1 + space1 + building_string_key + divider2 + space2 + construction_string_key
                                    value = house_string_value + ', ' + building_string_value + ', ' + construction_string_value
                                    self.houses_buildings_constructions[key] = value

        result = True
        for key, value in self.houses_buildings_constructions.items():
            sn = Building(key)
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != value:
                    result = False
                    break
            #if not result:
            #print('*' + key + '*', '*' + sn.normalize() + '*', '*' + value + '*', result)
        self.assertTrue(result)


    def test_houses_constructions_buildings(self):
        print("Тест дом-строение-корпус")
        self.houses_constructions_buildings = {}

        #дом - строение - корпус
        #дом-строение-корпус отображается, так же, как дом-корпус-строение
        for house_string_key, house_string_value in self.house_test_strings.items():
            # два одинаковых цикла по разделителям и пробелам
            for divider1 in self.dividers:
                for space1 in self.spaces:
                    if (divider1 == ' ') and (space1 == ' '):
                        divider1 = ''
                    for building_string_key, building_string_value in self.building_test_strings.items():
                        for construction_string_key, construction_string_value in self.construction_test_strings.items():
                            # два одинаковых цикла по разделителям и пробелам
                            for divider2 in self.dividers:
                                for space2 in self.spaces:
                                    if (divider2 == ' ') and (space2 == ' '):
                                        divider2 = ''
                                    key = house_string_key + divider1 + space1 + building_string_key + divider2 + space2 + construction_string_key
                                    value = house_string_value + ', ' + building_string_value + ', ' + construction_string_value
                                    self.houses_constructions_buildings[key] = value
                                    
        result = True
        for key, value in self.houses_constructions_buildings.items():
            sn = Building(key)
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != value:
                    result = False
                    break

            #if not result:
            #print('*' + key + '*', '*' + sn.normalize() + '*', '*' + value + '*', result)
        self.assertTrue(result)


        



    def _est_houses_buildings_comments(self):
        print("Тест дома-корпус-комментарий")
        self.houses_constructions_comments = {}
        
        # дом - корпус - комментарий
        # комментарий не выводится
        for house_string_key, house_string_value in self.house_test_strings.items():
            # два одинаковых цикла по разделителям и пробелам
            for divider1 in self.dividers:
                for space1 in self.spaces:
                    if (divider1 == ' ') and (space1 == ' '):
                        divider1 = ''
                    for building_string_key, building_string_value in self.building_test_strings.items():
                        for comment in self.comments:
                            # два одинаковых цикла по разделителям и пробелам
                            for divider2 in self.dividers:
                                for space2 in self.spaces:
                                    if (divider2 == ' ') and (space2 == ' '):
                                        divider2 = ''
                                    key = house_string_key + divider1 + space1 + building_string_key + divider2 + space2 + comment
                                    value = house_string_value + ', ' + building_string_value
                                    self.houses_buildings_comments[key] = value

        result = True
        for key, value in self.houses_buildings_comments.items():
            sn = Building(key)
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != value:
                    result = False
                    break

        #if not result:
        #    print('*' + key + '*', '*' + sn.normalize() + '*', '*' + value + '*', result)
        self.assertTrue(result)


    def _est1(self):
        print("test1")
        '''for house_string in self.house_test_strings:
            # два одинаковых цикла по разделителям и пробелам
            for divider1 in self.dividers:
                for space1 in self.spaces:
                    if (divider1 == ' ') and (space1 == ' '):
                        divider1 = ''
                    for building_string in self.building_test_strings:
                        for construction_string in self.construction_test_strings:
                            # два одинаковых цикла по разделителям и пробелам
                            for divider2 in self.dividers:
                                for space2 in self.spaces:
                                    if (divider2 == ' ') and (space2 == ' '):
                                        divider2 = ''
                                    #print(house_string + divider1 + space1 + construction_string + divider2 + space2 + building_string)
                                    self.houses_constructions_buildings.append(house_string + divider1 + space1 + construction_string + divider2 + space2 + building_string)'''


        # дом - корпус - комментарий
        '''for house_string in self.house_test_strings:
            for building_string in self.building_test_strings:
                for divider1 in self.dividers:
                    for space1 in self.spaces:
                        if (divider1 == ' ') and (space1 == ' '):
                            divider1 = ''
                        for comment in self.comments:
                            for divider2 in self.dividers:
                                for space2 in self.spaces:
                                    if (divider2 == ' ') and (space2 == ' '):
                                        divider2 = ''
                                    #print(house_string + divider1 + space1 + building_string + divider2 + space2 + comment)
                                    self.houses_buildings_comments.append(house_string + divider1 + space1 + building_string + divider2 + space2 + comment)'''
                    
        #дом - строение - комментарий
        '''for house_string in self.house_test_strings:
            for construction_string in self.construction_test_strings:
                for divider1 in self.dividers:
                    for space1 in self.spaces:
                        if (divider1 == ' ') and (space1 == ' '):
                            divider1 = ''
                        for comment in self.comments:
                            for divider2 in self.dividers:
                                for space2 in self.spaces:
                                    if (divider2 == ' ') and (space2 == ' '):
                                        divider2 = ''
                                    #print(house_string + divider1 + space1 + construction_string + divider2 + space2 + comment)
                                    self.houses_constructions_comments.append(house_string + divider1 + space1 + construction_string + divider2 + space2 + comment)'''

        #дом - корпус - строение - комментарий
        '''for house_string in self.house_test_strings:
            # два одинаковых цикла по разделителям и пробелам
            for divider1 in self.dividers:
                for space1 in self.spaces:
                    if (divider1 == ' ') and (space1 == ' '):
                        divider1 = ''
                    for building_string in self.building_test_strings:
                        for construction_string in self.construction_test_strings:
                            # два одинаковых цикла по разделителям и пробелам
                            for divider2 in self.dividers:
                                for space2 in self.spaces:
                                    if (divider2 == ' ') and (space2 == ' '):
                                        divider2 = ''
                                    for comment in self.comments:
                                        for divider3 in self.dividers:
                                            for space3 in self.spaces:
                                                if (divider3 == ' ') and (space3 == ' '):
                                                    divider3 = ''
                                                #print(house_string + divider1 + space1 + building_string + divider2 + space2 + construction_string + divider3 + space3 + comment)
                                                self.houses_buildings_constructions_comments.append(house_string + divider1 + space1 + building_string + divider2 + space2 + construction_string + divider3 + space3 + comment)'''

        #дом - строение - корпус - комментарий
        '''for house_string in self.house_test_strings:
            # два одинаковых цикла по разделителям и пробелам
            for divider1 in self.dividers:
                for space1 in self.spaces:
                    if (divider1 == ' ') and (space1 == ' '):
                        divider1 = ''
                    for building_string in self.building_test_strings:
                        for construction_string in self.construction_test_strings:
                            # два одинаковых цикла по разделителям и пробелам
                            for divider2 in self.dividers:
                                for space2 in self.spaces:
                                    if (divider2 == ' ') and (space2 == ' '):
                                        divider2 = ''
                                    for comment in self.comments:
                                        for divider3 in self.dividers:
                                            for space3 in self.spaces:
                                                if (divider3 == ' ') and (space3 == ' '):
                                                    divider3 = ''
                                                #print(house_string + divider1 + space1 + construction_string + divider2 + space2 + building_string + divider3 + space3 + comment)
                                                self.houses_constructions_buildings_comments.append(house_string + divider1 + space1 + construction_string + divider2 + space2 + building_string + divider3 + space3 + comment)'''
                                                
        #print(len(self.house_test_strings) + len(self.building_test_strings) + len(self.construction_test_strings) + len(self.houses_buildings) + len(self.houses_constructions) + len(self.houses_buildings_constructions) + len(self.houses_buildings_constructions) + len(self.houses_constructions_buildings) + len(self.houses_constructions_buildings) + len(self.houses_buildings_comments) + len(self.houses_constructions_comments) + len(self.houses_buildings_constructions_comments) + len(self.houses_constructions_buildings_comments))
        
        '''
        #дом - строение - комментарий
        for item in self.houses_constructions_comments:
            print(item)
        #дом - корпус - строение - комментарий
        for item in self.houses_buildings_constructions_comments:
            print(item)
        #дом - строение - корпус - комментарий
        for item in self.houses_constructions_buildings_comments:
            print(item)'''
            
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()
    
