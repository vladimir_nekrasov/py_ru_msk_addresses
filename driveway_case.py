# -*- coding: utf-8 -*-
import unittest
from AddressAbstractCase import AbstractAddressTest
from Address import Address

'''
Тесты переулков
'''

###
### Для того, чтобы работало автоматическое создание тестовых случаев,
### числительное ставится в формате '2-й' или '20-й'
###


class DrivewayAddressTest(AbstractAddressTest):
    subst_word = 'проезд'

    abbrev = [
        'пр.',
        'пр'
    ]

    one_digit_numerals = [
        '2-й',
        '2й',
    ]

    two_digit_numerals = [
        '20-й',
        '20й'
    ]

    test1_src_strings = [
        'Загорьевский проезд',
        'проезд Загорьевский'
    ]

    test2_src_strings = [
        '{numeral} Боткинский проезд',
        '{numeral} проезд Боткинский',
        'Боткинский {numeral} проезд',
        'Боткинский проезд {numeral}',
        'проезд {numeral} Боткинский',
        'проезд Боткинский {numeral}'
    ]

    test3_src_strings = [
        '{numeral} Верхний Михайловский проезд',
        '{numeral} проезд Верхний Михайловский',
        'Верхний Михайловский {numeral} проезд',
        'Верхний Михайловский проезд {numeral}',
        'проезд Верхний Михайловский {numeral}',
        'проезд {numeral} Верхний Михайловский'
    ]

    # Проектируемый проезд N4386

    #
    # Формат "проезд Загорьевский"
    #
    def test1(self):
        src = self.test1_src_strings
        test = "проезд Загорьевский"

        print('Тест \"' + test + '\"')

        test_strings = []

        # замена сокращений
        for item in src:
            test_strings.append(item)
            test_strings.extend(self.abbreviations(item))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, sn.lane_name, sn.is_recognized, sn.is_simple_lane, '-' + sn.normalize() + '-', '-' + test + '-')
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "проезд 2-й Боткинский"
    #
    def test2(self):
        src = self.test2_src_strings
        test = "проезд 2-й Боткинский"

        print('Тест \"' + test + '\"')

        # подстановка числительных
        test_strings =[]
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.one_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item))
        test_strings.extend(tmp)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "проезд 20-й Боткинский"
    #
    def test3(self):
        src = self.test2_src_strings
        test = "проезд 20-й Боткинский"

        print('Тест \"' + test + '\"')

        # подстановка числительных
        test_strings = []
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.two_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "проезд 2-й Верхний Михайловский"
    #
    def test4(self):
        src = self.test3_src_strings
        test = "проезд 2-й Верхний Михайловский"

        print('Тест \"' + test + '\"')

        # подстановка числительных
        test_strings = []
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.one_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "проезд 20-й Верхний Михайловский"
    #
    def test5(self):
        src = self.test3_src_strings
        test = "проезд 20-й Верхний Михайловский"

        print('Тест \"' + test + '\"')

        # подстановка числительных
        test_strings = []
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.two_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()

