# -*- coding: utf-8 -*-
import unittest
from AddressAbstractCase import AbstractAddressTest
from Address import Address

'''
Тесты шоссе
'''


class HighwayAddressTest(AbstractAddressTest):
    subst_word_1 = 'шоссе'
    subst_word_2 = 'дублёр'

    abbrev1 = [
        'ш.',
        'ш'
    ]

    abbrev2 = [
        'дублер',
        'дубл.',
        'дубл',
        'дуб.',
        'дуб'
    ]

    one_digit_numerals_a = [
        '2-е',
        '2е',
    ]

    one_digit_numerals_b = [
        '2-го',
        '2го',
    ]

    two_digit_numerals_a = [
        '20-е',
        '20е'
    ]

    two_digit_numerals_b = [
        '20-го',
        '20го'
    ]

    test1_src_strings = [
        'Хорошевское шоссе',
        'шоссе Хорошевское'
    ]

    test2_src_strings = [
        'дублёр Алтуфьевского шоссе',
        'дублёр шоссе Алтуфьевского',
        'Алтуфьевского шоссе дублёр',
        'шоссе Алтуфьевского дублёр',
    ]

    test3_src_strings = [
        '{numeral} Успенское шоссе',
        '{numeral} шоссе Успенское',
        'Успенское {numeral} шоссе',
        'шоссе {numeral} Успенское',
        'шоссе Успенское {numeral}',
        'Успенское шоссе {numeral}'
    ]

    test4_src_strings = [
        'дублёр {numeral} Успенского шоссе',
        'дублёр Успенского {numeral} шоссе',
        'дублёр Успенского шоссе {numeral}',
        'дублёр шоссе {numeral} Успенского',
        'дублёр шоссе Успенского {numeral}',
        'дублёр {numeral} шоссе Успенского',
        '{numeral} Успенского шоссе дублёр',
        'Успенского {numeral} шоссе дублёр',
        'Успенского шоссе {numeral} дублёр',
        'шоссе {numeral} Успенского дублёр',
        'шоссе Успенского {numeral} дублёр',
        '{numeral} шоссе Успенского дублёр'
    ]

    #
    # Формат "шоссе Хорошевское"
    #
    def test1(self):
        src = self.test1_src_strings
        test = "шоссе Хорошевское"

        print('Тест \"' + test + '\"')

        test_strings = []
        test_strings.extend(src)

        # замена сокращений 'шоссе'
        tmp = []
        for item in src:
            test_strings.append(item)
            test_strings.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "дублёр Алтуфьевского шоссе"
    #
    def test2(self):
        src = self.test2_src_strings
        test = "дублёр Алтуфьевского шоссе"

        print('Тест \"' + test + '\"')

        test_strings = []

        # замена сокращений 'шоссе'
        tmp = []
        for item in src:
            test_strings.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))

        # замена сокращений 'дублёр'
        for item in tmp:
            test_strings.append(item)
            test_strings.append(self.brackets(item, self.subst_word_2))
            test_strings.extend(self.abbreviations(item, self.subst_word_2, self.abbrev2, with_brackets=True))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)


    #
    # Формат "шоссе 2-е Успенское"
    #
    def test3(self):
        src = self.test3_src_strings
        test = "шоссе 2-е Успенское"

        print('Тест \"' + test + '\"')

        test_strings = []

        # подстановка числительных
        tmp = []
        for item in src:
            tmp.extend(self.substitute_numerals(item, self.one_digit_numerals_a))
        test_strings.extend(tmp)

        # замена сокращений 'шоссе'
        tmp = []
        for item in test_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)


    #
    # Формат "шоссе 20-е Успенское"
    #
    def test4(self):
        src = self.test3_src_strings
        test = "шоссе 20-е Успенское"

        print('Тест \"' + test + '\"')

        test_strings = []

        # подстановка числительных
        tmp = []
        for item in src:
            tmp.extend(self.substitute_numerals(item, self.two_digit_numerals_a))
        test_strings.extend(tmp)

        # замена сокращений 'шоссе'
        tmp = []
        for item in test_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)


    #
    # Формат "дублёр 2-го Успенского шоссе"
    #
    def test5(self):
        src = self.test4_src_strings
        test = "дублёр 2-го Успенского шоссе"

        print('Тест \"' + test + '\"')

        test_strings = []

        # подстановка числительных
        tmp = []
        for item in src:
            tmp.extend(self.substitute_numerals(item, self.one_digit_numerals_b))
        test_strings.extend(tmp)
        #print(test_strings)

        # замена сокращений 'шоссе'
        tmp = []
        for item in test_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))
        test_strings.extend(tmp)
        #print(test_strings)

        # замена сокращений 'дублёр'
        for item in tmp:
            test_strings.append(item)
            test_strings.append(self.brackets(item, self.subst_word_2))
            test_strings.extend(self.abbreviations(item, self.subst_word_2, self.abbrev2, with_brackets=True))
        #print(test_strings)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "дублёр 2-го Успенского шоссе"
    #
    def test5(self):
        src = self.test4_src_strings
        test = "дублёр 2-го Успенского шоссе"

        print('Тест \"' + test + '\"')

        test_strings = []

        # подстановка числительных
        tmp = []
        for item in src:
            tmp.extend(self.substitute_numerals(item, self.one_digit_numerals_b))
        test_strings.extend(tmp)
        #print(test_strings)

        # замена сокращений 'шоссе'
        tmp = []
        for item in test_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))
        test_strings.extend(tmp)
        #print(test_strings)

        # замена сокращений 'дублёр'
        for item in tmp:
            test_strings.append(item)
            test_strings.append(self.brackets(item, self.subst_word_2))
            test_strings.extend(self.abbreviations(item, self.subst_word_2, self.abbrev2, with_brackets=True))
        #print(test_strings)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)



    #
    # Формат "дублёр 20-го Успенского шоссе"
    #
    def test6(self):
        src = self.test4_src_strings
        test = "дублёр 20-го Успенского шоссе"

        print('Тест \"' + test + '\"')

        test_strings = []

        # подстановка числительных
        tmp = []
        for item in src:
            tmp.extend(self.substitute_numerals(item, self.two_digit_numerals_b))
        test_strings.extend(tmp)
        #print(test_strings)

        # замена сокращений 'шоссе'
        tmp = []
        for item in test_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word_1, self.abbrev1))
        test_strings.extend(tmp)
        #print(test_strings)

        # замена сокращений 'дублёр'
        for item in tmp:
            test_strings.append(item)
            test_strings.append(self.brackets(item, self.subst_word_2))
            test_strings.extend(self.abbreviations(item, self.subst_word_2, self.abbrev2, with_brackets=True))
        #print(test_strings)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)




if __name__ == '__main__':
    unittest.main()

