# -*- coding: utf-8 -*-
import unittest
from AddressAbstractCase import AbstractAddressTest
from Address import Address

'''
Тесты переулков
'''

###
### Для того, чтобы работало автоматическое создание тестовых случаев,
### числительное ставится в формате '2-й' или '20-й' (только эти числа)
###


class LaneAddressTest(AbstractAddressTest):
    subst_word = 'переулок'

    abbrev = [
        'переул.',
        'переул',
        'пер.',
        'пер'
    ]

    one_digit_numerals = [
        '2 - й',
        '2- й',
        '2 -й',
        '2-й',
        '2 - ой',
        '2- ой',
        '2 -ой',
        '2-ой',
        '2й',
        '2 й',
        '2ой',
        '2 ой'
    ]

    one_digit_numerals_a = [
        '3 - й',
        '3- й',
        '3 -й',
        '3-й',
        '3 - ий',
        '3- ий',
        '3 -ий',
        '3-ий',
        '3й',
        '3 й',
        '3 ий',
        '3ий'
    ]

    two_digit_numerals = [
        '20 - ый',
        '20- ый',
        '20 -ый',
        '20-ый',
        '20 - й',
        '20- й',
        '20 -й',
        '20-й',
        '20й',
        '20 й',
        '20ый',
        '20 ый'
    ]

    test1_src_strings = [
        'переулок Александра Невского',
        'Александра Невского переулок'
    ]

    test2_src_strings = [
        '{numeral} Полевой переулок',
        'Полевой {numeral} переулок',
        'Полевой переулок {numeral}',
        'переулок {numeral} Полевой'
    ]

    test3_src_strings = [
        '{numeral} Тверской-Ямской переулок',
        'Тверской-Ямской {numeral} переулок',
        'Тверской-Ямской переулок {numeral}',
        'переулок {numeral} Тверской-Ямской'
    ]

    #
    # Формат "переулок Александра Невского"
    #
    def test1(self):
        src = self.test1_src_strings
        test = "переулок Александра Невского"

        print('Тест \"' + test + '\"')

        test_strings = []

        # замена сокращений
        for item in src:
            test_strings.append(item)
            test_strings.extend(self.abbreviations(item, self.subst_word, self.abbrev))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, sn.lane_name, sn.is_recognized, sn.is_simple_lane, '-' + sn.normalize() + '-', '-' + test + '-')
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "переулок 2-й Полевой"
    #
    def test2(self):
        src = self.test2_src_strings
        test = "переулок 2-й Полевой"

        print('Тест \"' + test + '\"')

        # подстановка числительных
        test_strings =[]
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.one_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev))
        test_strings.extend(tmp)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "переулок 3-й Полевой"
    #
    def test3(self):
        src = self.test2_src_strings
        test = "переулок 3-й Полевой"

        print('Тест \"' + test + '\"')

        # подстановка числительных
        test_strings =[]
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.one_digit_numerals_a))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev))
        test_strings.extend(tmp)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "переулок 20-й Полевой"
    #
    def test4(self):
        src = self.test2_src_strings
        test = "переулок 20-й Полевой"

        print('Тест \"' + test + '\"')

        # подстановка числительных
        test_strings = []
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.two_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)


    #
    # Формат "переулок 2-й Тверской-Ямской"
    #
    def test5(self):
        src = self.test3_src_strings
        test = "переулок 2-й Тверской-Ямской"

        print('Тест \"' + test + '\"')

        # подстановка числительных
        test_strings = []
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.one_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "переулок 20-й Тверской-Ямской"
    #
    def test6(self):
        src = self.test3_src_strings
        test = "переулок 20-й Тверской-Ямской"

        print('Тест \"' + test + '\"')

        # подстановка числительных
        test_strings = []
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.two_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string)
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if not sn.is_recognized:
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()

