# -*- coding: utf-8 -*-
import unittest
from AddressAbstractCase import AbstractAddressTest
from Address import Address
from Metro import moscow_metro_stations_words, init_metro_dict

'''
Тест станций метро
'''


class MetroAddressTest(AbstractAddressTest):

    def setUp(self):
        self.metro_tests = init_metro_dict(moscow_metro_stations_words)

    def test1(self):
        src = self.metro_tests

        test_strings = dict()

        for key, value in self.metro_tests.items():
            test_strings[key] = value

        # Подстановка сокращений
        tmp_dict = dict()
        for key, value in test_strings.items():
            tmp_dict['метро ' + key] = value
            tmp_dict['метро ' + key] = value
            tmp_dict['ст.м. ' + key] = value
            tmp_dict['ст м ' + key] = value
            tmp_dict['ст м. ' + key] = value
            tmp_dict['ст.м ' + key] = value
            tmp_dict['ст. м. ' + key] = value
            tmp_dict['ст м. ' + key] = value
            tmp_dict['ст. м. ' + key] = value
            tmp_dict['м. ' + key] = value
            tmp_dict['м ' + key] = value
            tmp_dict[key + ' метро'] = value
            tmp_dict[key + ' ст.м.'] = value
            tmp_dict[key + ' ст м'] = value
            tmp_dict[key + ' ст м.'] = value
            tmp_dict[key + ' ст.м'] = value
            tmp_dict[key + ' ст. м.'] = value
            tmp_dict[key + ' ст м.'] = value
            tmp_dict[key + ' ст. м.'] = value
            tmp_dict[key + ' м.'] = value
            tmp_dict[key + ' м'] = value
        test_strings.update(tmp_dict)

        # Подстановка пробелов
        tmp = dict()
        for key, value in test_strings.items():
            tmp[self.spaces(key)] = value
        test_strings.update(tmp)

        print(len(test_strings))
        print(test_strings)

        result = True

        # Цикл по тестовым случаям
        for test_string, test_sample in test_strings.items():
            # В параметр передаётся список написаний станций метро
            sn = Address(test_string, self.metro_tests)
            sn.parse()
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != 'м. ' + test_sample:
                    result = False
                    break
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()


