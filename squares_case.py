# -*- coding: utf-8 -*-
import unittest
from AddressAbstractCase import AbstractAddressTest
from Address import Address

'''
Тесты площадей
'''


class SquaresAddressTest(AbstractAddressTest):
    subst_word = 'площадь'

    abbrev = [
        'пл.',
        'пл'
    ]

    test1_src_strings = [
        'Манежная площадь',
        'площадь Манежная'
    ]

    test2_src_strings = [
        'Курского вокзала площадь',
        'площадь Курского вокзала'
    ]

    test3_src_strings = [
        ('Площадь Белорусского вокзала', 'Белорусский вокзал'),
        ('Площадь Казанского вокзала', 'Казанский вокзал'),
        ('Площадь Киевского вокзала', 'Киевский вокзал'),
        ('Площадь Курского вокзала', 'Курский вокзал'),
        ('Площадь Ленинградского вокзала', 'Ленинградский вокзал'),
        ('Площадь Павелецкого вокзала', 'Павелецкий вокзал'),
        ('Площадь Рижского вокзала', 'Рижский вокзал'),
        ('Площадь Савёловского вокзала', 'Савёловский вокзал'),
        ('Площадь Ярославского вокзала', 'Ярославский вокзал')
    ]

    #
    # Формат "площадь Манежная"
    #
    def test1(self):
        src = self.test1_src_strings
        test = "площадь Манежная"

        print('Тест \"' + test + '\"')

        test_strings = []

        # замена сокращений
        for item in src:
            test_strings.append(item)
            test_strings.extend(self.abbreviations(item, self.subst_word, self.abbrev))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "площадь Курского вокзала"
    #
    def test2(self):
        src = self.test2_src_strings
        test = "площадь Курского вокзала"

        print('Тест \"' + test + '\"')

        test_strings = []
        test_strings.extend(src)

        # замена сокращений
        tmp = []
        for item in src:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev))
        test_strings.extend(tmp)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print(len(test_strings))

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)
        
    def test3(self):
        src = []
        test = []
        
        print('Тест определения вокзалов')
        
        for item in self.test3_src_strings:
            src.append(item[0])
            test.append(item[1])
        #print(src)
        #print(test)
        
        src_strings = []
        test_strings = []
        src_strings.extend(src)
        test_strings.extend(test)
        
        # замена сокращений
        tmp = []
        tmp_test = []
        for i in range(len(src)):
            abbr = self.abbreviations(src[i], self.subst_word, self.abbrev)
            tmp.extend(abbr)
            tmp_test.extend([test[i]]*len(abbr))
        src_strings.extend(tmp)
        test_strings.extend(tmp_test)
        
        #дополнение пробелами
        tmp = []
        for item in src_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        src_strings.extend(tmp)
        test_strings.extend(test_strings)
        
        # Проверка результата
        stations_results = []
        for src_string in src_strings:
            sn = Address(src_string)
            sn.parse()
            if sn.is_near_the_station:
                stations_results.append(sn.station)

        #print(len(stations_results), len(test_strings))
        #for i in range(len(stations_results)):
            #print(stations_results[i], test_strings[i])
                
        self.assertListEqual(stations_results, test_strings)
            


if __name__ == '__main__':
    unittest.main()

