# -*- coding: utf-8 -*-
import unittest
from AddressAbstractCase import AbstractAddressTest
from Address import Address

'''
Тесты переулков
'''


class StationAddressTest(AbstractAddressTest):
    subst_word = 'вокзал'

    abbrev = [
        'вкз.',
        'вкз'
    ]

    test1_src_strings = [
        'Павелецкий вокзал',
        'вокзал Павелецкий'
    ]

    #
    # Формат "вокзал Павелецкий"
    #
    def test1(self):
        src = self.test1_src_strings
        test = "Павелецкий вокзал"

        print('Тест \"' + test + '\"')

        test_strings = []

        # замена сокращений
        for item in src:
            test_strings.append(item)
            test_strings.extend(self.abbreviations(item, self.subst_word, self.abbrev))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            #print(test_string)
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.normalize() + '-')
            if not sn.is_recognized:
                #print(test_string)
                result = False
                break
            else:
                #print(test_string, '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()

