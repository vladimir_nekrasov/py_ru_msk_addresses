# -*- coding: utf-8 -*-
import unittest
from AddressAbstractCase import AbstractAddressTest
from Address import Address

'''
Тесты улиц
'''


class StreetAddressTest(AbstractAddressTest):
    subst_word = 'улица'
    subst_word2 = 'года'

    abbrev1 = [
        'ул.',
        'ул'
    ]

    abbrev2 = [
        'г.',
        'г'
    ]

    one_digit_numerals = [
        '2-я',
        '2я',
    ]

    two_digit_numerals = [
        '20-я',
        '20я'
    ]

    test1_src_strings = [
        'улица Ширшова',
        'Ширшова улица'
    ]

    test2_src_strings = [
        '{numeral} Парковая улица',
        'Парковая {numeral} улица',
        'Парковая улица {numeral}',
        'улица {numeral} Парковая',
        'улица Парковая {numeral}'
    ]

    test3_src_strings = [
        u'{numeral} улица Соколиной горы',
        u'{numeral} Соколиной горы улица',
        u'Соколиной горы {numeral} улица',
        u'Соколиной горы улица {numeral}',
        u'улица {numeral} Соколиной горы',
        u'улица Соколиной горы {numeral}'
    ]

    test4_src_strings = [
        'улица 1905 года',
        'улица 1905-го года',
        'улица 1905го года',
        '1905 года улица',
        '1905-го года улица',
        '1905го года улица',
        'улица года 1905',
        'улица года 1905-го',
        'улица года 1905го',
        'года 1905 улица',
        'года 1905-го улица',
        'года 1905го улица'
    ]

    #
    # Формат "улица Ширшова"
    #
    def test1(self):
        src = self.test1_src_strings
        test = "улица Ширшова"

        test_strings = []

        # замена сокращений
        for item in src:
            test_strings.append(item)
            test_strings.extend(self.abbreviations(item, self.subst_word, self.abbrev1))

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            tmp.append(self.spaces(item))
        test_strings.extend(tmp)

        print('Тест \"' + test + '\",', len(test_strings), 'комбинаций')

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            if not sn.is_recognized:
                result = False
                break
            else:
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "улица 2-я Парковая"
    #
    def test2(self):
        src = self.test2_src_strings
        test = "улица 2-я Парковая"

        # подстановка числительных
        test_strings =[]
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.one_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev1))
        test_strings.extend(tmp)

        #дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print('Тест \"' + test + '\",', len(test_strings), 'комбинаций')

        # Проверка результата
        result = True
        for test_string in test_strings:
            #print(test_string)
            sn = Address(test_string)
            sn.parse()
            if not sn.is_recognized:
                result = False
                break
            else:
                #print('-' + test_string + '-', '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "улица 20-я Парковая"
    #
    def test3(self):
        src = self.test2_src_strings
        test = "улица 20-я Парковая"

        # подстановка числительных
        test_strings = []
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.two_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev1))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print('Тест \"' + test + '\",', len(test_strings), 'комбинаций')

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if (not sn.is_recognized):
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)


    #
    # Формат "улица 2-я Соколиной горы"
    #
    def test4(self):
        src = self.test3_src_strings
        test = "улица 2-я Соколиной горы"

        # подстановка числительных
        test_strings = []
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.one_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev1))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print('Тест \"' + test + '\",', len(test_strings), 'комбинаций')

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if not sn.is_recognized:
                result = False
                break
            else:
                #print('-' + test_string + '-', '-' + sn.normalize() + '-')
                if sn.normalize() != test:
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "улица 20-я Соколиной горы"
    #
    def test5(self):
        src = self.test3_src_strings
        test = "улица 20-я Соколиной горы"

        # подстановка числительных
        test_strings = []
        for item in src:
            test_strings.extend(self.substitute_numerals(item, self.two_digit_numerals))

        # замена сокращений
        tmp = []
        for item in test_strings:
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev1))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print('Тест \"' + test + '\",', len(test_strings), 'комбинаций')

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if not sn.is_recognized:
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)

    #
    # Формат "улица 1905 года"
    #
    def test6(self):
        src = self.test4_src_strings
        test = "улица 1905 года"

        test_strings = []

        # замена сокращений 'улица'
        tmp = []
        for item in self.test4_src_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word, self.abbrev1))
        test_strings.extend(tmp)

        # замена сокращений 'года'
        tmp = []
        for item in test_strings:
            tmp.append(item)
            tmp.extend(self.abbreviations(item, self.subst_word2, self.abbrev2))
        test_strings.extend(tmp)

        # дополнение пробелами
        tmp = []
        for item in test_strings:
            parts = item.split(' ')
            str_case = "     " + "     ".join(parts) + "     "
            tmp.append(str_case)
        test_strings.extend(tmp)

        print('Тест \"' + test + '\",', len(test_strings), 'комбинаций')
        #print(len(test_strings))
        #print(test_strings)

        # Проверка результата
        result = True
        for test_string in test_strings:
            sn = Address(test_string)
            sn.parse()
            #print(test_string, '-' + sn.lane_name + '-', '-' + sn.numeral + '-', sn.is_recognized, sn.is_simple_lane, sn.is_numeral_lane, '-' + str(sn.normalize()) + '-', '-' + test + '-')
            if not sn.is_recognized:
                result = False
                break
            else:
                if (sn.normalize() != test):
                    result = False
                    break

        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()













'''test_streets_1 = [
    'ул. Ширшова',
    'улица Ширшова',
    'ул Ширшова',
    'Ширшова ул',
    'Ширшова ул.',
    'Ширшова улица'
]

test_streets_2 = [
    '10-я Парковая улица',
    '10-я Парковая ул.',
    '10-я Парковая ул',
    '10я Парковая улица',
    '10я Парковая ул',
    '10я Парковая ул.',
    '10 Парковая улица',
    '10 Парковая ул.',
    '10 Парковая ул',
    'улица Парковая 10-я',
    'ул. Парковая 10-я',
    'ул Парковая 10-я',
    'улица Парковая 10я',
    'ул. Парковая 10я',
    'ул Парковая 10я',
    'улица Парковая 10',
    'ул. Парковая 10',
    'ул Парковая 10',
    'улица 10-я Парковая',
    'ул. 10-я Парковая',
    'ул 10-я Парковая',
    'улица 10я Парковая',
    'ул. 10я Парковая',
    'ул 10я Парковая',
    'улица 10 Парковая',
    'ул. 10 Парковая',
    'ул 10 Парковая',
]

test_streets_3 = [
    #улица
    u'10-я          улица           Соколиной горы',
    u'10я улица Соколиной горы',
    u'10 улица Соколиной горы',
    u'(10-я) улица Соколиной горы',
    u'(10я) улица Соколиной горы',
    u'(10) улица Соколиной горы',
    u'Соколиной горы 10-я улица',
    u'Соколиной горы 2-я улица',
    u'Соколиной горы 10я улица',
    u'Соколиной горы 10 улица',
    u'Соколиной горы (10-я) улица',
    u'Соколиной горы (10я) улица',
    u'Соколиной горы (10) улица',
    u'улица 10-я Соколиной горы',
    u'улица 10я Соколиной горы',
    u'улица 10 Соколиной горы',
    u'улица (10-я) Соколиной горы',
    u'улица (10я) Соколиной горы',
    u'улица (10) Соколиной горы',
    u'улица Соколиной горы 10-я',
    u'улица Соколиной горы 10я',
    u'улица Соколиной горы 10',
    u'улица Соколиной горы (10-я)',
    u'улица Соколиной горы (10я)',
    u'улица Соколиной горы (10)',
    u'10-я Соколиной горы улица',
    u'10я Соколиной горы улица',
    u'10 Соколиной горы улица',
    u'(10-я) Соколиной горы улица',
    u'(10я) Соколиной горы улица',
    u'(10) Соколиной горы улица',
    #ул.
    u'10-я ул. Соколиной горы',
    u'10я ул. Соколиной горы',
    u'10 ул. Соколиной горы',
    u'(10-я) ул. Соколиной горы',
    u'(10я) ул. Соколиной горы',
    u'(10) ул. Соколиной горы',
    u'Соколиной горы 10-я ул.',
    u'Соколиной горы 10я ул.',
    u'Соколиной горы 10 ул.',
    u'Соколиной горы (10-я) ул.',
    u'Соколиной горы (10я) ул.',
    u'Соколиной горы (10) ул.',
    u'ул. 10-я Соколиной горы',
    u'ул. 10я Соколиной горы',
    u'ул. 10 Соколиной горы',
    u'ул. (10-я) Соколиной горы',
    u'ул. (10я) Соколиной горы',
    u'ул. (10) Соколиной горы',
    u'ул. Соколиной горы 10-я',
    u'ул. Соколиной горы 10я',
    u'ул. Соколиной горы 10',
    u'ул. Соколиной горы (10-я)',
    u'ул. Соколиной горы (10я)',
    u'ул. Соколиной горы (10)',
    u'10-я Соколиной горы ул.',
    u'10я Соколиной горы ул.',
    u'10 Соколиной горы ул.',
    u'(10-я) Соколиной горы ул.',
    u'(10я) Соколиной горы ул.',
    u'(10) Соколиной горы ул.',
    #ул
    u'10-я ул Соколиной горы',
    u'10я ул Соколиной горы',
    u'10 ул Соколиной горы',
    u'(10-я) ул Соколиной горы',
    u'(10я) ул Соколиной горы',
    u'(10) ул Соколиной горы',
    u'Соколиной горы 10-я ул',
    u'Соколиной горы 10я ул',
    u'Соколиной горы 10 ул',
    u'Соколиной горы (10-я) ул',
    u'Соколиной горы (10я) ул',
    u'Соколиной горы (10) ул',
    u'ул 10-я Соколиной горы',
    u'ул 10я Соколиной горы',
    u'ул 10 Соколиной горы',
    u'ул (10-я) Соколиной горы',
    u'ул (10я) Соколиной горы',
    u'ул (10) Соколиной горы',
    u'ул Соколиной горы 10-я',
    u'ул Соколиной горы 10я',
    u'ул Соколиной горы 10',
    u'ул Соколиной горы (10-я)',
    u'ул Соколиной горы (10я)',
    u'ул Соколиной горы (10)',
    u'10-я Соколиной горы ул',
    u'10я Соколиной горы ул',
    u'10 Соколиной горы ул',
    u'(10-я) Соколиной горы ул',
    u'(10я) Соколиной горы ул',
    u'(10) Соколиной горы ул'
]

test_streets_4 = [
    #улица
    u'10-я          улица           Парковая',
    u'10я улица Парковая',
    u'10 улица Парковая',
    u'(10-я) улица Парковая',
    u'(10я) улица Парковая',
    u'(10) улица Парковая',
    u'Парковая 10-я улица',
    u'Парковая 2-я улица',
    u'Парковая 10я улица',
    u'Парковая 10 улица',
    u'Парковая (10-я) улица',
    u'Парковая (10я) улица',
    u'Парковая (10) улица',
    u'улица 10-я Парковая',
    u'улица 10я Парковая',
    u'улица 10 Парковая',
    u'улица (10-я) Парковая',
    u'улица (10я) Парковая',
    u'улица (10) Парковая',
    u'улица Парковая 10-я',
    u'улица Парковая 10я',
    u'улица Парковая 10',
    u'улица Парковая (10-я)',
    u'улица Парковая (10я)',
    u'улица Парковая (10)',
    u'10-я Парковая улица',
    u'10я Парковая улица',
    u'10 Парковая улица',
    u'(10-я) Парковая улица',
    u'(10я) Парковая улица',
    u'(10) Парковая улица',
    #ул.
    u'10-я ул. Парковая',
    u'10я ул. Парковая',
    u'10 ул. Парковая',
    u'(10-я) ул. Парковая',
    u'(10я) ул. Парковая',
    u'(10) ул. Парковая',
    u'Парковая 10-я ул.',
    u'Парковая 10я ул.',
    u'Парковая 10 ул.',
    u'Парковая (10-я) ул.',
    u'Парковая (10я) ул.',
    u'Парковая (10) ул.',
    u'ул. 10-я Парковая',
    u'ул. 10я Парковая',
    u'ул. 10 Парковая',
    u'ул. (10-я) Парковая',
    u'ул. (10я) Парковая',
    u'ул. (10) Парковая',
    u'ул. Парковая 10-я',
    u'ул. Парковая 10я',
    u'ул. Парковая 10',
    u'ул. Парковая (10-я)',
    u'ул. Парковая (10я)',
    u'ул. Парковая (10)',
    u'10-я Парковая ул.',
    u'10я Парковая ул.',
    u'10 Парковая ул.',
    u'(10-я) Парковая ул.',
    u'(10я) Парковая ул.',
    u'(10) Парковая ул.',
    #ул
    u'10-я ул Парковая',
    u'10я ул Парковая',
    u'10 ул Парковая',
    u'(10-я) ул Парковая',
    u'(10я) ул Парковая',
    u'(10) ул Парковая',
    u'Парковая 10-я ул',
    u'Парковая 10я ул',
    u'Парковая 10 ул',
    u'Парковая (10-я) ул',
    u'Парковая (10я) ул',
    u'Парковая (10) ул',
    u'ул 10-я Парковая',
    u'ул 10я Парковая',
    u'ул 10 Парковая',
    u'ул (10-я) Парковая',
    u'ул (10я) Парковая',
    u'ул (10) Парковая',
    u'ул Парковая 10-я',
    u'ул Парковая 10я',
    u'ул Парковая 10',
    u'ул Парковая (10-я)',
    u'ул Парковая (10я)',
    u'ул Парковая (10)',
    u'10-я Парковая ул',
    u'10я Парковая ул',
    u'10 Парковая ул',
    u'(10-я) Парковая ул',
    u'(10я) Парковая ул',
    u'(10) Парковая ул'
]

for item in test_streets_2:
    sn = Address(item)
    print(item, '-' + sn.street_name + '-', '-' + sn.numeral + '-', '-' + sn.normalize() + '-', sn.is_simple_street, sn.is_numeral_street)'''
